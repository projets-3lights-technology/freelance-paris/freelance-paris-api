name := "freelance-paris"

version := "1.0"

lazy val `freelance-paris` = (project in file(".")).enablePlugins(PlayScala)

resolvers += "scalaz-bintray" at "https://dl.bintray.com/scalaz/releases"

resolvers += "Akka Snapshot Repository" at "http://repo.akka.io/snapshots/"

scalaVersion := "2.12.8"
scalacOptions += "-Ypartial-unification"

lazy val scalazVersion = "7.2.27"
lazy val doobieVersion = "0.6.0"
lazy val catsVersion = "1.5.0"

libraryDependencies ++= Seq(
  guice,
  jdbc,
  ehcache,
  ws,
  "org.scalaz" %% "scalaz-core" % scalazVersion,
  "org.postgresql" % "postgresql" % "42.2.5",
  "org.tpolecat" %% "doobie-core" % doobieVersion,
  "org.tpolecat" %% "doobie-postgres" % doobieVersion,
  "org.tpolecat" %% "doobie-specs2" % doobieVersion,
  "org.typelevel" %% "cats-core" % catsVersion,
  "javax.mail" % "mail" % "1.4",
  "org.scalatestplus.play" %% "scalatestplus-play" % "3.1.2" % Test,
  "org.scalamock" %% "scalamock" % "4.1.0" % Test,
  "de.leanovate.play-mockws" %% "play-mockws" % "2.6.6" % Test,
  "io.cucumber" %% "cucumber-scala" % "4.2.0" % Test,
  "io.cucumber" % "cucumber-junit" % "4.2.0" % Test,
  "io.cucumber" % "cucumber-guice" % "4.2.0" % Test,
  "junit" % "junit" % "4.12" % Test,
  "com.waioeka.sbt" %% "cucumber-runner" % "0.1.5" % Test
)

testFrameworks += new TestFramework("com.waioeka.sbt.runner.CucumberFramework")

parallelExecution in Test := true

unmanagedResourceDirectories in Test += baseDirectory.value / "target/web/public/test"

