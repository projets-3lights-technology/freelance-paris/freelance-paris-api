package commoncontext.unit

import commoncontext.adapters.gateways.inmemory.InMemoryHealthChecker
import commoncontext.usecases.CheckHealth
import org.scalatest.Matchers._
import org.scalatestplus.play.PlaySpec

class HealthCheckSpec extends PlaySpec {

  "Check up" in {
    val healthChecker = new InMemoryHealthChecker()
    val checkHealth: CheckHealth = new CheckHealth(healthChecker)

    checkHealth.check should be("UP")
  }

  "Check down" in {
    val healthChecker = new InMemoryHealthChecker("down")
    val checkHealth: CheckHealth = new CheckHealth(healthChecker)

    checkHealth.check should be("DOWN")
  }


}
