package commoncontext.unit

import commoncontext.domain.entities.AuthenticatedUser

object DummyAuthenticatedUser {

  def create(id: String = "authenticatedUser",
             firstName: String = "first name",
             lastName: String = "last name",
             email: String = "email",
             avatar: String = "avatar",
             profileUrl: String = "profile url"): AuthenticatedUser = AuthenticatedUser(
    id = id,
    firstName = firstName,
    lastName = lastName,
    email = email,
    avatar = avatar,
    profileUrl = profileUrl
  )

}
