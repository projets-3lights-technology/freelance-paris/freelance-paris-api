package commoncontext.unit

import commoncontext.adapters.gateways.inmemory.InMemoryIdentifierChecker
import commoncontext.domain.entities.{AuthenticatedUser, GuestUser}
import commoncontext.domain.gateways.IdentifierChecker
import commoncontext.usecases.Gatekeeper
import org.scalatest.BeforeAndAfterEach
import org.scalatest.Matchers._
import org.scalatestplus.play.PlaySpec
import scalaz.Scalaz._

class GatekeeperSpec extends PlaySpec with BeforeAndAfterEach {

  var gatekeeper: Gatekeeper = _

  override protected def beforeEach(): Unit = {
    val identifierLoader: IdentifierChecker = new InMemoryIdentifierChecker()
    gatekeeper = new Gatekeeper(identifierLoader)
  }

  "Access to authorized content" should {
    "Gatekeeper allows the entrance" in {
      val user = gatekeeper.accessToAuthorizedContent(Some("Bearer token"))
      user should be(AuthenticatedUser(
        id = "registeredUser",
        firstName = "first name",
        lastName = "last name",
        email = "email",
        avatar = "avatar",
        profileUrl = "profile url"
      ) success)
    }

    "Gatekeeper allows guest entrance" in {
      val user = gatekeeper.accessToAuthorizedContent(Some("Bearer guest"))
      user should be(GuestUser success)
    }

    "Gatekeeper rejects the entrance" in {
      val user = gatekeeper.accessToAuthorizedContent(Some("Bearer 1234"))
      user should be("Unauthorized" failure)

      val user2 = gatekeeper.accessToAuthorizedContent(None)
      user2 should be("Unauthorized" failure)
    }
  }

  "Access to authenticated content" should {
    "Gatekeeper allows the entrance for authenticate user" in {
      val user = gatekeeper.accessToAuthenticatedContent(Some("Bearer token"))
      user should be(AuthenticatedUser(
        id = "registeredUser",
        firstName = "first name",
        lastName = "last name",
        email = "email",
        avatar = "avatar",
        profileUrl = "profile url"
      ) success)
    }

    "Gatekeeper disallows the entrance for guest user" in {
      val user = gatekeeper.accessToAuthenticatedContent(Some("Bearer guest"))
      user should be("Forbidden" failure)    }

    "Gatekeeper rejects the entrance" in {
      val user = gatekeeper.accessToAuthenticatedContent(Some("Bearer 1234"))
      user should be("Unauthorized" failure)

      val user2 = gatekeeper.accessToAuthenticatedContent(None)
      user2 should be("Unauthorized" failure)
    }
  }

}
