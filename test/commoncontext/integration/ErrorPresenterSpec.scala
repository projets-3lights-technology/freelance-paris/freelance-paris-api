package commoncontext.integration

import commoncontext.adapters.controllers.presenters.ErrorPresenter._
import commoncontext.adapters.controllers.viewmodels.ErrorVM
import org.scalatest.Matchers._
import org.scalatestplus.play.PlaySpec

class ErrorPresenterSpec extends PlaySpec {

  "Present bad request error" in {
    val message: String = "error"

    val error: ErrorVM = to400ErrorVM(message)

    error should be(ErrorVM(
      "Bad Request",
      "error"
    ))
  }

  "Present conflict error" in {
    val message: String = "error"

    val error: ErrorVM = to409ErrorVM(message)

    error should be(ErrorVM(
      "Conflict",
      "error"
    ))
  }

}
