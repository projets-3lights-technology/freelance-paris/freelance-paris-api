package commoncontext.integration

import commoncontext.adapters.gateways.real.DTOs.LinkedInPeopleDTO
import commoncontext.adapters.gateways.real.LinkedInIdentifierChecker
import commoncontext.domain.entities.AuthenticatedUser
import configuration.InMemoryAppConfiguration
import mockws.MockWSHelpers._
import mockws._
import org.scalamock.scalatest.MockFactory
import org.scalatest.Matchers._
import org.scalatestplus.play.PlaySpec
import play.api.libs.json._
import play.api.mvc.Results._
import play.api.test.Helpers._


class LinkedInIdentifierCheckerSpec extends PlaySpec with MockFactory {

  private implicit val personReads: Writes[LinkedInPeopleDTO] = Json.writes[LinkedInPeopleDTO]

  "Call Linked In" in {
    val route = Route {
      case (GET, "LINKEDIN_URL/people/~:(id,first-name,last-name,email-address,picture-url,public-profile-url)") => Action {
        Ok(Json.toJson(LinkedInPeopleDTO(
          id = "id",
          firstName = "firstname",
          lastName = "lastname",
          emailAddress = "email",
          pictureUrl = "avatar",
          publicProfileUrl = "public profile"
        )))
      }
    }
    val wsClientMock = MockWS(route)
    val linkedInIdentifierChecker = new LinkedInIdentifierChecker(wsClientMock, InMemoryAppConfiguration)
    val result = linkedInIdentifierChecker.check("token")

    route.called should be(true)
    result should be(Some(AuthenticatedUser(
      id = "id",
      firstName = "firstname",
      lastName = "lastname",
      email = "email",
      avatar = "avatar",
      profileUrl = "public profile"
    )))
    wsClientMock.close()
  }

}
