package missioncontext.integration

import java.util.UUID

import commoncontext.unit.DummyAuthenticatedUser
import membercontext.DummyMember
import missioncontext.adapters.controllers.mappers.MissionCommandMappers._
import missioncontext.adapters.controllers.validators.ContactMissionForm
import missioncontext.{DummyAuthor, DummyMissionDetails, DummyRecipient}
import org.scalatest.Matchers._
import org.scalatestplus.play.PlaySpec

class MissionContactSpec extends PlaySpec {

  "Create contact mission command" in {
    val contactMissionCommand = toContactMissionCommand(
      mission = DummyMissionDetails.create(),
      author = DummyAuthenticatedUser.create(),
      recipient = DummyMember.create(),
      contactMissionForm = ContactMissionForm(
        subject = " subject ",
        message = " message "
      )
    )

    contactMissionCommand.missionId should be(UUID.fromString("a-b-c-d-e"))
    contactMissionCommand.missionName should be("Name")
    contactMissionCommand.author should be(DummyAuthor.create(
      id = "authenticatedUser",
      firstName = "first name",
      lastName = "last name",
      email = "email",
      profileUrl = "profile url"
    ))
    contactMissionCommand.recipient should be(DummyRecipient.create(
      id = "id",
      firstName = "firstName",
      lastName = "lastName",
      email = "email"
    ))
    contactMissionCommand.subject should be("subject")
    contactMissionCommand.message should be("message")
  }

}
