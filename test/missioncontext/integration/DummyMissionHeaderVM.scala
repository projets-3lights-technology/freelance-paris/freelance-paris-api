package missioncontext.integration

import java.util.UUID

import missioncontext.adapters.controllers.viewmodels.MissionHeaderVM

object DummyMissionHeaderVM {

  def create(id: UUID = UUID.fromString("a-b-c-d-e"),
             name: String = "Name",
             description: String = "description",
             average_daily_rate: Int = 123,
             author_id: String = "authorId"): MissionHeaderVM = MissionHeaderVM(
    id = id,
    name = name,
    description = description,
    average_daily_rate = average_daily_rate,
    author_id = author_id
  )

}
