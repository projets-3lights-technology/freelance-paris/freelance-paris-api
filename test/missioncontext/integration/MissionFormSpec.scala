package missioncontext.integration

import java.text.SimpleDateFormat
import java.util.UUID

import missioncontext.adapters.controllers.validators.MissionForm
import missioncontext.domain.enum.ContractType
import org.joda.time.DateTime
import org.scalatest.Matchers._
import org.scalatestplus.play.PlaySpec

class MissionFormSpec extends PlaySpec {

  "Create publish mission command" in {
    val missionForm = MissionForm(
      id = UUID.fromString("1-2-3-4-5"),
      name = " name ",
      description = " description ",
      average_daily_rate = 123,
      customer = " customer ",
      location = " location ",
      start_date = new SimpleDateFormat("yyyy-MM-dd").parse("2018-12-22"),
      duration_in_months = " -1 ",
      remote_is_possible = true,
      contract_type = ContractType.FREELANCE
    )

    val publishMissionCommand = missionForm.toPublishMissionCommand("authorId")
    publishMissionCommand.id should be(UUID.fromString("1-2-3-4-5"))
    publishMissionCommand.name should be("name")
    publishMissionCommand.description should be("description")
    publishMissionCommand.averageDailyRate should be(123)
    publishMissionCommand.customer should be("customer")
    publishMissionCommand.location should be("location")
    publishMissionCommand.startDate should be(DateTime.parse("2018-12-22"))
    publishMissionCommand.durationInMonths should be("-1")
    publishMissionCommand.remoteIsPossible should be(true)
    publishMissionCommand.authorId should be("authorId")
    publishMissionCommand.contractType should be(ContractType.FREELANCE)
  }

}
