package missioncontext.integration

import java.util.UUID

import missioncontext.{DummyMissionDetails, DummyMissionHeader}
import missioncontext.adapters.controllers.presenters.MissionPresenter._
import missioncontext.adapters.controllers.viewmodels.{MissionDetailsVM, MissionHeaderVM}
import missioncontext.domain.entities.{MemberId, MissionDetails, MissionHeader}
import org.scalatest.Matchers._
import org.scalatestplus.play.PlaySpec

class MissionPresenterSpec extends PlaySpec {

  "Present missions" in {
    val missionsHeader: List[MissionHeader] = List(
      DummyMissionHeader.create(),
      DummyMissionHeader.create(UUID.fromString("a-b-c-d-2"), "mission")
    )

    val missionHeaderViewModel: List[MissionHeaderVM] = missionsHeader

    missionHeaderViewModel should be(List(
      DummyMissionHeaderVM.create(),
      DummyMissionHeaderVM.create(UUID.fromString("a-b-c-d-2"), "mission")
    ))
  }

  "Present mission" should {
    "Not contacted by the member" in {
      val missionDetails: Option[MissionDetails] = Some(
        DummyMissionDetails.create(),
      )

      val missionDetailsViewModel: Option[MissionDetailsVM] = toMissionDetailsVM(missionDetails, "memberId")

      missionDetailsViewModel should be(Some(
        DummyMissionDetailsVM.create(),
      ))
    }

    "Contacted by the member" in {
      val missionDetails: Option[MissionDetails] = Some(
        DummyMissionDetails.create(contactedBy = List(MemberId("memberId"))),
      )

      val missionDetailsViewModel: Option[MissionDetailsVM] = toMissionDetailsVM(missionDetails, "memberId")

      missionDetailsViewModel should be(Some(
        DummyMissionDetailsVM.create(already_contacted = true),
      ))
    }
  }

}
