package missioncontext.integration

import java.util.UUID

import missioncontext.adapters.controllers.viewmodels.MissionDetailsVM
import missioncontext.domain.enum.ContractType.{FREELANCE, Type}

object DummyMissionDetailsVM {

  def create(id: UUID = UUID.fromString("a-b-c-d-e"),
             name: String = "Name",
             description: String = "description",
             average_daily_rate: Int = 123,
             customer: String = "customer",
             location: String = "location",
             start_date: String = "2018-11-25T15:34:01.000+01:00",
             duration_in_months: String = "1",
             remote_is_possible: Boolean = false,
             contract_type: Type = FREELANCE,
             already_contacted: Boolean = false): MissionDetailsVM = MissionDetailsVM(
    id = id,
    name = name,
    description = description,
    average_daily_rate = average_daily_rate,
    customer = customer,
    location = location,
    start_date = start_date,
    duration_in_months = duration_in_months,
    remote_is_possible = remote_is_possible,
    contract_type = contract_type,
    already_contacted = already_contacted
  )

}
