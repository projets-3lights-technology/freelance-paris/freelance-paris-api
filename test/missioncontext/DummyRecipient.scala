package missioncontext

import missioncontext.domain.entities.Recipient

object DummyRecipient {

  def create(id: String = "recipientId",
             firstName: String = "reci",
             lastName: String = "pient",
             email: String = "recipient email"): Recipient = Recipient(
    id = id,
    firstName = firstName,
    lastName = lastName,
    email = email,
  )

}
