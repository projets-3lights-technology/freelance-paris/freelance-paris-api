package missioncontext

import java.util.UUID

import missioncontext.domain.commands.PublishMissionCommand
import missioncontext.domain.enum.ContractType.{FREELANCE, Type}
import org.joda.time.DateTime

object DummyPublishMissionCommand {

  def create(id: UUID = UUID.fromString("a-b-c-d-e"),
             name: String = "Name",
             description: String = "description",
             averageDailyRate: Int = 123,
             customer: String = "customer",
             location: String = "location",
             startDate: DateTime = DateTime.parse("2018-11-25T15:34:01.000+01:00"),
             durationInMonths: String = "1",
             remoteIsPossible: Boolean = false,
             authorId: String = "authorId",
             contractType: Type = FREELANCE,
             createdAt: DateTime = DateTime.now()): PublishMissionCommand = PublishMissionCommand(
    id = id,
    name = name,
    description = description,
    averageDailyRate = averageDailyRate,
    customer = customer,
    location = location,
    startDate = startDate,
    durationInMonths = durationInMonths,
    remoteIsPossible = remoteIsPossible,
    authorId = authorId,
    contractType = contractType,
    createdAt = createdAt
  )

}
