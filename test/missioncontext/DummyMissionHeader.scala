package missioncontext

import java.util.UUID

import missioncontext.domain.entities.MissionHeader
import org.joda.time.DateTime

object DummyMissionHeader {

  def create(id: UUID = UUID.fromString("a-b-c-d-e"),
             name: String = "Name",
             description: String = "description",
             averageDailyRate: Int = 123,
             authorId: String = "authorId",
             startDate: DateTime = DateTime.now(),
             createdAt: DateTime = DateTime.now()): MissionHeader = MissionHeader(
    id = id,
    name = name,
    description = description,
    averageDailyRate = averageDailyRate,
    authorId = authorId,
    startDate = startDate,
    createdAt = createdAt
  )

}
