package missioncontext.unit

import java.util.UUID

import commoncontext.domain.handlers.CommandHandler
import missioncontext.adapters.gateways.inmemory.{InMemoryEmailNotifier, InMemoryMissionContactStore}
import missioncontext.domain.commands.ContactMissionCommand
import missioncontext.domain.entities.{Author, ContactItem, MissionContact, Recipient}
import missioncontext.usecases.commands.ContactMission
import org.joda.time.DateTime
import org.scalatest.Matchers._
import org.scalatestplus.play.PlaySpec

class MissionContactSpec extends PlaySpec {

  "Contact for a mission" in {
    val now = DateTime.now()
    val contactNotifier: InMemoryEmailNotifier = new InMemoryEmailNotifier()
    val missionContactStore: InMemoryMissionContactStore = new InMemoryMissionContactStore()
    val contactMission: CommandHandler[ContactMissionCommand] = new ContactMission(contactNotifier, missionContactStore)

    val author = Author(
      id = "authorId",
      firstName = "au",
      lastName = "thor",
      email = "author email",
      profileUrl = "profile url"
    )
    val recipient = Recipient(
      id = "recipientId",
      firstName = "recip",
      lastName = "ient",
      email = "recipient email"
    )
    val contactMissionCommand: ContactMissionCommand = ContactMissionCommand(
      missionId = UUID.fromString("a-b-c-d-e"),
      missionName = "mission name",
      author = author,
      recipient = recipient,
      subject = "subject",
      message = "message",
      sentAt = now
    )

    contactMission.handle(contactMissionCommand)


    val expectedMessage =
      """
        | <p>au thor vous a contacté pour la mission suivante : mission name</p>
        |
        | <p>Son profile LinkedIn : profile url</p>
        |
        | <span>---</span>
        |
        | <p>message</p>
        |
        | <p>Vous pouvez directement répondre à au thor en répondant à ce message</p>
        |
        | <p>Cordialement,</p>
        | <b>Freelance Paris</b>
      """.stripMargin

    contactNotifier.notifyWith should be(ContactItem(
      subject = "[mission name]",
      message = expectedMessage,
      author = author,
      recipient = recipient
    ))
    missionContactStore.storeWith should be(MissionContact(
      missionId = UUID.fromString("a-b-c-d-e"),
      authorId = "authorId",
      subject = "[mission name]",
      message = expectedMessage,
      authorEmail = "author email",
      recipientEmail = "recipient email",
      sentAt = now
    ))
  }

}
