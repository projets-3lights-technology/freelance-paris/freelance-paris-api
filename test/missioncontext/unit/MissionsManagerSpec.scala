package missioncontext.unit

import java.util.UUID

import commoncontext.domain.handlers.CommandHandler
import missioncontext.adapters.gateways.inmemory.InMemoryMissionRepository
import missioncontext.domain.commands.{PublishMissionCommand, RemoveMissionCommand}
import missioncontext.domain.entities.{MemberId, MissionDetails, MissionHeader}
import missioncontext.domain.enum.ContractType._
import missioncontext.domain.gateways.{CommandMissionRepository, ReadMissionRepository}
import missioncontext.usecases.commands.{PublishMission, RemoveMission}
import missioncontext.usecases.queries.MissionReadModel
import missioncontext.{DummyMissionDetails, DummyMissionHeader, DummyPublishMissionCommand}
import org.joda.time.DateTime
import org.scalatest.BeforeAndAfterEach
import org.scalatest.Matchers._
import org.scalatestplus.play.PlaySpec
import scalaz.Scalaz._

class MissionsManagerSpec extends PlaySpec with BeforeAndAfterEach {

  var commandMissionRepository: CommandMissionRepository = _
  var readMissionRepository: ReadMissionRepository = _
  var missionReadModel: MissionReadModel = _
  val someMissionsHeader: Set[MissionHeader] = Set(
    DummyMissionHeader.create(
      id = UUID.fromString("d-f-d-a-c"),
      startDate = DateTime.parse("2018-12-22T15:34:01.000+01:00"),
      createdAt = DateTime.parse("2018-11-22T15:34:01.000+01:00").minusDays(1)
    ),
    DummyMissionHeader.create(
      id = UUID.fromString("9-8-7-6-5"),
      startDate = DateTime.parse("2018-12-21T15:34:01.000+01:00"),
      createdAt = DateTime.parse("2018-11-22T15:34:01.000+01:00").minusDays(2)
    ),
    DummyMissionHeader.create(
      id = UUID.fromString("1-2-3-4-5"),
      startDate = DateTime.parse("2018-12-23T15:34:01.000+01:00"),
      createdAt = DateTime.parse("2018-11-22T15:34:01.000+01:00").minusDays(3)
    ),
  )
  val someMissionsDetails: Set[MissionDetails] = Set(
    DummyMissionDetails.create(UUID.fromString("d-f-d-a-c")),
    DummyMissionDetails.create(UUID.fromString("9-8-7-6-5"), contactedBy = List(MemberId("member1"), MemberId("member2"))),
    DummyMissionDetails.create(UUID.fromString("1-2-3-4-5")),
  )

  override def beforeEach(): Unit = {
    val missionRepository = new InMemoryMissionRepository(someMissionsHeader, someMissionsDetails)
    commandMissionRepository = missionRepository
    readMissionRepository = missionRepository
    missionReadModel = new MissionReadModel(readMissionRepository)
  }

  "Fetch zero mission if there is no missions" in {
    readMissionRepository = new InMemoryMissionRepository()
    missionReadModel = new MissionReadModel(readMissionRepository)
    missionReadModel.fetchStartedMissionStrictlyAfter(DateTime.parse("2018-12-23")) should be(List.empty)
  }

  "Publish a mission" when {
    "Which is unknown" in {
      val publishMission: CommandHandler[PublishMissionCommand] = new PublishMission(commandMissionRepository)
      val mission1CreationDate = DateTime.now()
      val mission2CreationDate = DateTime.now().plusDays(1)

      val result1 = publishMission.handle(DummyPublishMissionCommand.create(
        name = "mission",
        createdAt = mission1CreationDate
      ))
      result1 should be(true success)

      val result2 = publishMission.handle(DummyPublishMissionCommand.create(
        UUID.fromString("a-b-c-d-2"),
        createdAt = mission2CreationDate
      ))
      result2 should be(true success)

      missionReadModel.fetchStartedMissionStrictlyAfter(DateTime.parse("2018-11-25")) should contain theSameElementsInOrderAs (List(
        MissionHeader(
          id = UUID.fromString("a-b-c-d-2"),
          name = "Name",
          description = "description",
          averageDailyRate = 123,
          authorId = "authorId",
          startDate = DateTime.parse("2018-11-25T15:34:01.000+01:00"),
          createdAt = mission2CreationDate
        ),
        MissionHeader(
          id = UUID.fromString("a-b-c-d-e"),
          name = "mission",
          description = "description",
          averageDailyRate = 123,
          authorId = "authorId",
          startDate = DateTime.parse("2018-11-25T15:34:01.000+01:00"),
          createdAt = mission1CreationDate
        )
      ) ++ someMissionsHeader)
      missionReadModel.getMissionById(UUID.fromString("a-b-c-d-2")) should be(Some(MissionDetails(
        id = UUID.fromString("a-b-c-d-2"),
        name = "Name",
        description = "description",
        averageDailyRate = 123,
        customer = "customer",
        location = "location",
        startDate = DateTime.parse("2018-11-25T15:34:01.000+01:00"),
        durationInMonths = "1",
        remoteIsPossible = false,
        authorId = "authorId",
        contractType = FREELANCE,
        contactedBy = List.empty
      )))
    }

    "Which already exists" in {
      val publishMission: CommandHandler[PublishMissionCommand] = new PublishMission(commandMissionRepository)
      val result = publishMission.handle(DummyPublishMissionCommand.create(id = UUID.fromString("9-8-7-6-5")))

      missionReadModel.fetchStartedMissionStrictlyAfter(DateTime.parse("2018-12-21")).size should be(someMissionsHeader.size)
      result should be(UUID.fromString("9-8-7-6-5") + " already exists" failure)
    }
  }

  "Fetch started mission strictly after a date" in {
    missionReadModel.fetchStartedMissionStrictlyAfter(DateTime.parse("2018-12-23")).size should be(1)
    missionReadModel.fetchStartedMissionStrictlyAfter(DateTime.parse("2018-12-23")) should be(List(
      DummyMissionHeader.create(
        id = UUID.fromString("1-2-3-4-5"),
        startDate = DateTime.parse("2018-12-23T15:34:01.000+01:00"),
        createdAt = DateTime.parse("2018-11-22T15:34:01.000+01:00").minusDays(3)
      )
    ))
  }

  "Fetch the details" when {
    "The mission exists" in {
      missionReadModel.getMissionById(UUID.fromString("9-8-7-6-5")) should be(Some(MissionDetails(
        id = UUID.fromString("9-8-7-6-5"),
        name = "Name",
        description = "description",
        averageDailyRate = 123,
        customer = "customer",
        location = "location",
        startDate = DateTime.parse("2018-11-25T15:34:01.000+01:00"),
        durationInMonths = "1",
        remoteIsPossible = false,
        authorId = "authorId",
        contractType = FREELANCE,
        contactedBy = List(MemberId("member1"), MemberId("member2"))
      )))
    }

    "The mission does not exist" in {
      missionReadModel.getMissionById(UUID.fromString("5-6-4-7-3")) should be(None)
    }
  }

  "Remove a mission" when {
    "The author delete it" in {
      val removeMissionCommand: RemoveMissionCommand = RemoveMissionCommand(
        missionId = UUID.fromString("9-8-7-6-5"),
        missionAuthorId = "authorId",
        memberRequestId = "authorId"
      )
      val result = new RemoveMission(commandMissionRepository).handle(removeMissionCommand)
      result should be(true success)

      missionReadModel.getMissionById(UUID.fromString("9-8-7-6-5")) should be(None)
      missionReadModel.fetchStartedMissionStrictlyAfter(DateTime.parse("2018-12-21")).size should be(2)
    }

    "Is allowed only for its author" in {
      val removeMissionCommand: RemoveMissionCommand = RemoveMissionCommand(
        missionId = UUID.fromString("9-8-7-6-5"),
        missionAuthorId = "authorId",
        memberRequestId = "memberId"
      )
      val result = new RemoveMission(commandMissionRepository).handle(removeMissionCommand)
      result should be("The member isn't the author of this mission" failure)

      missionReadModel.fetchStartedMissionStrictlyAfter(DateTime.parse("2018-12-21")).size should be(3)
    }
  }

}
