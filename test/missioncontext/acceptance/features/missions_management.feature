Feature: Mission management

  Scenario Outline: Should view all update missions which are published and the details of each
    Given published missions
      | id        | created_at                    | name     | description | average_daily_rate | customer | location | start_date                    | duration_in_months | remote_is_possible | author_id | contract_type  |
      | a-b-c-d-2 | 2018-11-18T15:34:01.000+01:00 | mission2 | lorem ipsum | 123                | customer | location | 2018-12-18T15:34:01.000+01:00 | 1                  | false              | 1         | apprenticeship |
      | a-b-c-d-1 | 2018-11-18T15:34:00.000+01:00 | mission1 | lorem ipsum | 456                |          | location | 2018-12-18T15:34:00.000+01:00 | +12                | true               | 12        | cdd            |
      | a-b-c-d-3 | 2018-11-17T15:34:01.000+01:00 | mission3 | lorem ipsum | 235                | customer | location | 2018-12-17T15:34:01.000+01:00 | 4                  | false              | 4         | cdi            |
      | a-b-c-d-4 | 2018-11-25T15:34:01.000+01:00 | mission4 | lorem ipsum | 987                |          | location | 2018-12-25T15:34:01.000+01:00 | -1                 | true               | 1         | freelance      |
    When bob wants to see all missions strictly after 2018-12-18
    Then bob sees 3 missions
    When bob wants to see the details of the mission <id>
    Then bob sees missions from the most recent to the older one
      | id        | created_at                    | name     | description | average_daily_rate | start_date                    | author_id |
      | a-b-c-d-4 | 2018-11-25T15:34:01.000+01:00 | mission4 | lorem ipsum | 987                | 2018-12-25T15:34:01.000+01:00 | 1         |
      | a-b-c-d-2 | 2018-11-18T15:34:01.000+01:00 | mission2 | lorem ipsum | 123                | 2018-12-18T15:34:01.000+01:00 | 1         |
      | a-b-c-d-1 | 2018-11-18T15:34:00.000+01:00 | mission1 | lorem ipsum | 456                | 2018-12-18T15:34:00.000+01:00 | 12        |
    Then bob sees the details of the mission <id> named <name> published by <author_id> which starts <start_date> during <duration_in_months> months for <customer> at <location> has a description <description>. The mission is payed <average_daily_rate>, it's for <contract_type> and the remote is <remote_is_possible>
    Examples:
      | id        | name     | description | average_daily_rate | customer | location | start_date                    | duration_in_months | remote_is_possible | author_id | contract_type  |
      | a-b-c-d-1 | mission1 | lorem ipsum | 456                |          | location | 2018-12-18T15:34:00.000+01:00 | +12                | true               | 12        | cdd            |
      | a-b-c-d-2 | mission2 | lorem ipsum | 123                | customer | location | 2018-12-18T15:34:01.000+01:00 | 1                  | false              | 1         | apprenticeship |
      | a-b-c-d-3 | mission3 | lorem ipsum | 235                | customer | location | 2018-12-17T15:34:01.000+01:00 | 4                  | false              | 4         | cdi            |
      | a-b-c-d-4 | mission4 | lorem ipsum | 987                |          | location | 2018-12-25T15:34:01.000+01:00 | -1                 | true               | 1         | freelance      |

  Scenario Outline: Should publish a mission
    Given published missions
      | id        | created_at                    | name     | description | average_daily_rate | customer | location | start_date                    | duration_in_months | remote_is_possible | author_id | contract_type |
      | a-b-c-d-2 | 2018-11-18T15:34:01.000+01:00 | mission2 | lorem ipsum | 123                | customer | location | 2018-12-18T15:34:01.000+01:00 | 1                  | false              | 1         | interim       |
    When bob publishes the mission <id>
    When bob wants to see all missions strictly after 2018-11-18
    Then bob sees <total_missions> missions
    Examples:
      | id        | total_missions |
      | a-b-c-d-1 | 2              |
      | a-b-c-d-2 | 1              |

  Scenario Outline: Should remove only member's mission
    Given published missions
      | id        | created_at                    | name     | description | average_daily_rate | customer | location | start_date                    | duration_in_months | remote_is_possible | author_id | contract_type |
      | a-b-c-d-1 | 2018-11-18T15:34:01.000+01:00 | mission2 | lorem ipsum | 123                | customer | location | 2018-12-18T15:34:01.000+01:00 | 1                  | false              | 1         | portage       |
      | a-b-c-d-2 | 2018-11-18T15:34:00.000+01:00 | mission1 | lorem ipsum | 456                |          | location | 2018-12-18T15:34:00.000+01:00 | +12                | true               | 12        | other         |
    When bob removes the mission <id> of <author_id>
    When bob wants to see all missions strictly after 2018-11-18
    Then bob sees <nb_missions> missions
    Examples:
      | id        | author_id | nb_missions |
      | a-b-c-d-1 | 1         | 1           |
      | a-b-c-d-2 | 12        | 2           |