Feature: Mission contact

  Background:
    Given published missions
      | id        | created_at                    | name     | description | average_daily_rate | customer | location | start_date                    | duration_in_months | remote_is_possible | author_id | contract_type  |
      | a-b-c-d-2 | 2018-11-18T15:34:01.000+01:00 | mission2 | lorem ipsum | 123                | customer | location | 2018-12-18T15:34:01.000+01:00 | 1                  | false              | 1         | apprenticeship |
      | a-b-c-d-1 | 2018-11-18T15:34:00.000+01:00 | mission1 | lorem ipsum | 456                |          | location | 2018-12-18T15:34:00.000+01:00 | +12                | true               | 12        | cdd            |
      | a-b-c-d-3 | 2018-11-17T15:34:01.000+01:00 | mission3 | lorem ipsum | 235                | customer | location | 2018-12-17T15:34:01.000+01:00 | 4                  | false              | 4         | cdi            |
      | a-b-c-d-4 | 2018-11-25T15:34:01.000+01:00 | mission4 | lorem ipsum | 987                |          | location | 2018-12-25T15:34:01.000+01:00 | -1                 | true               | 1         | freelance      |

  Scenario Outline: Inform the author when someone contact him for one of his missions
    Given bob who is a member
    When bob contact for mission <id> he writes <object> and <message>
    Then the author is notified with <object_sent> and <message_sent>
    Examples:
      | id        | object | message     | object_sent | message_sent |
      | a-b-c-d-2 | object | lorem ispum | object      | lorem ipsum  |