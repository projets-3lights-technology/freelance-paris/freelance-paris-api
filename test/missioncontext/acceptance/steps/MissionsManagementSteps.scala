package missioncontext.acceptance.steps

import java.util.UUID

import com.waioeka.sbt.runner.CucumberSpec
import configuration.WorldObject
import cucumber.api.scala.{EN, ScalaDsl}
import io.cucumber.datatable.DataTable
import missioncontext.domain.commands.RemoveMissionCommand
import missioncontext.domain.entities.{MissionDetails, MissionHeader}
import missioncontext.domain.enum.ContractType._
import missioncontext.{DummyMissionDetails, DummyMissionHeader, DummyPublishMissionCommand}
import org.joda.time.DateTime
import org.scalatest.Matchers._
import org.scalatestplus.play.PlaySpec

class MissionsManagementSteps extends PlaySpec with CucumberSpec with ScalaDsl with EN {

  var worldObject: WorldObject = _

  var missions: List[MissionHeader] = _
  var mission: Option[MissionDetails] = _

  Before() { _ => worldObject = new WorldObject() }

  Given("""^published missions$""") { missionsDT: DataTable =>
    missionsDT.asMaps[String, String](classOf[String], classOf[String]).forEach(raw => {
      val publishMissionCommand = DummyPublishMissionCommand.create(
        id = UUID.fromString(raw.get("id")),
        name = raw.get("name"),
        description = raw.get("description"),
        averageDailyRate = raw.get("average_daily_rate").toInt,
        customer = raw.getOrDefault("customer", ""),
        location = raw.get("location"),
        startDate = DateTime.parse(raw.get("start_date")),
        durationInMonths = raw.get("duration_in_months"),
        remoteIsPossible = raw.get("remote_is_possible").toBoolean,
        authorId = raw.get("author_id"),
        contractType = raw.get("contract_type").toString,
        createdAt = DateTime.parse(raw.get("created_at"))
      )
      worldObject.publishMission.handle(publishMissionCommand)
    })

    missionsDT.cells().get(0).size() should be(12)
  }

  When("""^bob wants to see all missions strictly after (.*?)$""") { date: String =>
    missions = worldObject.missionReadModel fetchStartedMissionStrictlyAfter DateTime.parse(date)
  }

  When("""^bob wants to see the details of the mission (.*?)$""") { missionId: String =>
    mission = worldObject.missionReadModel getMissionById UUID.fromString(missionId)
  }

  When("""^bob publishes the mission (.*?)$""") { missionId: String =>
    val publishMissionCommand = DummyPublishMissionCommand.create(
      id = UUID.fromString(missionId),
      name = "mission3",
      description = "lorem ipsum",
      averageDailyRate = 456,
      customer = "customer2",
      location = "location2",
      startDate = DateTime.parse("2018-12-18T15:34:01.000+01:00"),
      durationInMonths = "12",
      remoteIsPossible = true,
      authorId = "1",
      contractType = "interim",
    )
    worldObject.publishMission.handle(publishMissionCommand)
  }

  When("""^bob removes the mission (.*?) of (.*?)$""") { (missionId: String, authorId: String) =>
    val removeMissionCommand: RemoveMissionCommand = RemoveMissionCommand(
      missionId = UUID.fromString(missionId),
      missionAuthorId = authorId,
      memberRequestId = "1"
    )
    worldObject.removeMission.handle(removeMissionCommand)
  }

  Then("""^bob sees (.*?) missions$""") { missionCount: Int =>
    missions.size should be(missionCount)
  }

  Then("""^bob sees missions from the most recent to the older one$""") { orderedMissionsDT: DataTable =>
    var orderedMission: List[MissionHeader] = List.empty

    orderedMissionsDT.asMaps[String, String](classOf[String], classOf[String]).forEach(raw =>
      orderedMission = orderedMission :+ DummyMissionHeader.create(
        id = UUID.fromString(raw.get("id")),
        name = raw.get("name"),
        description = raw.get("description"),
        averageDailyRate = raw.get("average_daily_rate").toInt,
        authorId = raw.get("author_id"),
        startDate = DateTime.parse(raw.get("start_date")),
        createdAt = DateTime.parse(raw.get("created_at"))
      )
    )
    orderedMissionsDT.cells().get(0).size() should be(7)

    missions should contain theSameElementsInOrderAs orderedMission
  }

  Then("""^bob sees the details of the mission (.*?) named (.*?) published by (.*?) which starts (.*?) during (.*?) months for (.*?) at (.*?) has a description (.*?). The mission is payed (.*?), it's for (.*?) and the remote is (.*?)$""") {
    (missionId: String, missionName: String, authorId: String, startDate: String, durationInMonths: String, customer: String, location: String, description: String, averageDailyRate: Int, contractType: String, remoteIsPossible: String) =>

      val missionDetails = DummyMissionDetails.create(
        id = UUID.fromString(missionId),
        name = missionName,
        description = description,
        averageDailyRate = averageDailyRate,
        customer = customer,
        location = location,
        startDate = DateTime.parse(startDate),
        durationInMonths = durationInMonths,
        remoteIsPossible = remoteIsPossible.toBoolean,
        authorId = authorId,
        contractType = contractType
      )

      mission should be(Some(missionDetails))
  }

}
