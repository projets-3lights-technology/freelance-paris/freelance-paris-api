package missioncontext

import missioncontext.domain.entities.Author

object DummyAuthor {

  def create(id: String = "authorId",
             firstName: String = "au",
             lastName: String = "thor",
             email: String = "author email",
             profileUrl: String = "author profile"): Author = Author(
    id = id,
    firstName = firstName,
    lastName = lastName,
    email = email,
    profileUrl = profileUrl
  )

}
