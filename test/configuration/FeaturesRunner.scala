package configuration

import cucumber.api.CucumberOptions
import cucumber.api.junit.Cucumber
import org.junit.runner.RunWith

@RunWith(classOf[Cucumber])
@CucumberOptions(
  features = Array(
    "test/missioncontext/acceptance/features",
    "test/membercontext/acceptance/features"
  ),
  extraGlue = Array(
    "missioncontext.acceptance.steps",
    "membercontext.acceptance.steps"
  ),
  plugin = Array(
    "pretty",
    "html:test/reports/features"
  )
)
class FeaturesRunner
