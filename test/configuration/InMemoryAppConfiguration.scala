package configuration

import configuration.environment.{AppConfiguration, ConfigurationKey}
import configuration.environment.ConfigurationKey._

object InMemoryAppConfiguration extends AppConfiguration {

  override def get(key: ConfigurationKey.Type): Option[String] = key match {
    case LINKEDIN_URL => Some(key.concat("/"))
    case _ => Some(key)
  }

}
