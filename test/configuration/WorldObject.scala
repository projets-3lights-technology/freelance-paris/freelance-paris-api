package configuration

import commoncontext.adapters.gateways.inmemory.InMemoryIdentifierChecker
import commoncontext.domain.gateways.IdentifierChecker
import commoncontext.domain.handlers.CommandHandler
import commoncontext.usecases.Gatekeeper
import membercontext.adapters.gateways.inmemory.InMemoryMemberRepository
import membercontext.domain.commands.{CreateMemberCommand, UpdateMemberEmailCommand}
import membercontext.domain.gateways.{CommandMemberRepository, ReadMemberRepository}
import membercontext.usecases.MemberOrchestrator
import membercontext.usecases.commands.{CreateMember, UpdateMemberEmail}
import membercontext.usecases.queries.MemberReadModel
import missioncontext.adapters.gateways.inmemory.InMemoryMissionRepository
import missioncontext.domain.commands.{PublishMissionCommand, RemoveMissionCommand}
import missioncontext.domain.gateways.{CommandMissionRepository, ReadMissionRepository}
import missioncontext.usecases.commands.{PublishMission, RemoveMission}
import missioncontext.usecases.queries.MissionReadModel

class WorldObject {
  private val identifierChecker: IdentifierChecker = new InMemoryIdentifierChecker()
  val gatekeeper: Gatekeeper = new Gatekeeper(identifierChecker)

  private val missionRepository: InMemoryMissionRepository = new InMemoryMissionRepository()
  private val commandMissionRepository: CommandMissionRepository = missionRepository
  private val readMissionRepository: ReadMissionRepository = missionRepository

  val missionReadModel: MissionReadModel = new MissionReadModel(readMissionRepository)
  val publishMission: CommandHandler[PublishMissionCommand] = new PublishMission(commandMissionRepository)
  val removeMission: CommandHandler[RemoveMissionCommand] = new RemoveMission(commandMissionRepository)


  private val memberRepository: InMemoryMemberRepository = new InMemoryMemberRepository()
  private val commandMemberRepository: CommandMemberRepository = memberRepository
  private val readMemberRepository: ReadMemberRepository = memberRepository

  val memberReadModel: MemberReadModel = new MemberReadModel(readMemberRepository)
  val createMember: CommandHandler[CreateMemberCommand] = new CreateMember(commandMemberRepository)
  val updateMemberEmail: CommandHandler[UpdateMemberEmailCommand] = new UpdateMemberEmail(commandMemberRepository)
  val memberOrchestrator: MemberOrchestrator = new MemberOrchestrator(createMember, memberReadModel)
}
