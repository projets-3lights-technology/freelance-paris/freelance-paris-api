Feature: Member management

  Scenario Outline: Member gets his information for the first time
    Given a authorized user
    When member <id> wants to see his information
    Then member <id> is <first_name> - <last_name>, his email is <email>, his avatar is <avatar> and his profile can be see at <profile_url>
    Examples:
      | id             | first_name | last_name | email | avatar | profile_url |
      | registeredUser | first name | last name | email | avatar | profile url |

  Scenario Outline: Member gets his information after changing his email
    Given a authorized user
      | id             | first_name | last_name | email | avatar |
      | registeredUser | first name | last name | email | avatar |
    When member <id> changes his email for <email>
    When member <id> wants to see his information
    Then member <id> is <first_name> - <last_name>, his email is <email>, his avatar is <avatar> and his profile can be see at <profile_url>
    Examples:
      | id             | first_name | last_name | email               | avatar | profile_url |
      | registeredUser | first name | last name | new.email@gmail.com | avatar | profile url |
      | registeredUser | first name | last name | email               | avatar | profile url |