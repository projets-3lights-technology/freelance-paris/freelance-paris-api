package membercontext.acceptance.steps

import com.waioeka.sbt.runner.CucumberSpec
import commoncontext.domain.entities.AuthenticatedUser
import configuration.WorldObject
import cucumber.api.scala.{EN, ScalaDsl}
import membercontext.domain.commands.UpdateMemberEmailCommand
import membercontext.domain.entities.Member
import org.scalatest.Matchers._
import org.scalatestplus.play.PlaySpec

class MemberManagementSteps extends PlaySpec with CucumberSpec with ScalaDsl with EN {

  var worldObject: WorldObject = _

  var authenticatedUser: AuthenticatedUser = _
  var member: Option[Member] = _

  Before() { _ => worldObject = new WorldObject() }

  Given("""^a authorized user$""") { () =>
    worldObject.gatekeeper.accessToAuthorizedContent(Some("token"))
      .fold(
        _ => "unauthorized",
        worldObject.memberOrchestrator.handle
      )
  }

  When("""^member (.*?) wants to see his information$""") { memberId: String =>
    member = worldObject.memberReadModel.getMemberById(memberId)
  }

  When("""^member (.*?) changes his email for (.*?)$""") { (memberId: String, email: String) =>
    val updateMemberEmailCommand: UpdateMemberEmailCommand = UpdateMemberEmailCommand(memberId, email)
    worldObject.updateMemberEmail.handle(updateMemberEmailCommand)
  }

  Then("""^member (.*?) is (.*?) - (.*?), his email is (.*?), his avatar is (.*?) and his profile can be see at (.*?)$""") {
    (memberId: String, firstName: String, lastName: String, email: String, avatar: String, profileUrl: String) =>
      member should be(Some(Member(
        id = memberId,
        firstName = firstName,
        lastName = lastName,
        email = email,
        avatar = avatar,
        profileUrl = profileUrl
      )))
  }

}
