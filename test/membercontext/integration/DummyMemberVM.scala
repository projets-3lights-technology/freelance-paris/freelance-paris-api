package membercontext.integration

import membercontext.adapters.controllers.viewmodels.MemberVM

object DummyMemberVM {

  def create(id: String = "id",
             first_name: String = "firstName",
             last_name: String = "lastName",
             email: String = "email",
             avatar: String = "avatar"): MemberVM = MemberVM(
    id = id,
    first_name = first_name,
    last_name = last_name,
    email = email,
    avatar = avatar
  )

}
