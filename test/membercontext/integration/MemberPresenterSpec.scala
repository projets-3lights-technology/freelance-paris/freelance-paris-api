package membercontext.integration

import membercontext.DummyMember
import membercontext.adapters.controllers.presenters.MemberPresenter._
import membercontext.adapters.controllers.viewmodels.MemberVM
import membercontext.domain.entities.Member
import org.scalatest.Matchers._
import org.scalatestplus.play.PlaySpec

class MemberPresenterSpec extends PlaySpec {

  "Present member" in {
    val member: Member = DummyMember.create()

    val memberVM: MemberVM = member

    memberVM should be(DummyMemberVM.create())
  }

}
