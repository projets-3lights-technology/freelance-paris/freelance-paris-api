package membercontext.unit

import commoncontext.domain.handlers.CommandHandler
import commoncontext.unit.DummyAuthenticatedUser
import membercontext.adapters.gateways.inmemory.InMemoryMemberRepository
import membercontext.domain.commands.{CreateMemberCommand, UpdateMemberEmailCommand}
import membercontext.domain.entities.Member
import membercontext.domain.gateways.{CommandMemberRepository, ReadMemberRepository}
import membercontext.usecases.MemberOrchestrator
import membercontext.usecases.commands.{CreateMember, UpdateMemberEmail}
import membercontext.usecases.queries.MemberReadModel
import org.scalatest.BeforeAndAfterEach
import org.scalatest.Matchers._
import org.scalatestplus.play.PlaySpec
import scalaz.Scalaz._

class MemberOrchestratorSpec extends PlaySpec with BeforeAndAfterEach {

  var memberRepository: InMemoryMemberRepository = _
  var updateMemberEmail: CommandHandler[UpdateMemberEmailCommand] = _
  var memberOrchestrator: MemberOrchestrator = _

  override protected def beforeEach(): Unit = {
    memberRepository = new InMemoryMemberRepository()
    val commandMemberRepository: CommandMemberRepository = memberRepository
    val readMemberRepository: ReadMemberRepository = memberRepository
    val createMember: CommandHandler[CreateMemberCommand] = new CreateMember(commandMemberRepository)
    val memberReadModel: MemberReadModel = new MemberReadModel(readMemberRepository)
    updateMemberEmail = new UpdateMemberEmail(commandMemberRepository)
    memberOrchestrator = new MemberOrchestrator(createMember, memberReadModel)
  }

  "Stock authorized user information" in {
    val member = memberOrchestrator.handle(DummyAuthenticatedUser.create())

    val expectedMember = Member(
      id = "authenticatedUser",
      firstName = "first name",
      lastName = "last name",
      email = "email",
      avatar = "avatar",
      profileUrl = "profile url"
    )
    member should be(expectedMember)
    memberRepository.addWith should be(expectedMember)
  }

  "Change email of member" in {
    memberOrchestrator.handle(DummyAuthenticatedUser.create())
    updateMemberEmail.handle(UpdateMemberEmailCommand("authenticatedUser", "new.email@email.com"))
    val member = memberOrchestrator.handle(DummyAuthenticatedUser.create())

    val expectedMember = Member(
      id = "authenticatedUser",
      firstName = "first name",
      lastName = "last name",
      email = "new.email@email.com",
      avatar = "avatar",
      profileUrl = "profile url"
    )
    member should be(expectedMember)
    memberRepository.updateWith should be(expectedMember)
  }

  "Change email of unknown member" in {
    val result = updateMemberEmail.handle(UpdateMemberEmailCommand("authenticatedUser", "new.email@email.com"))
    result should be("Member doesn't exist" failure)
  }

  "Email is invalid" in {
    memberOrchestrator.handle(DummyAuthenticatedUser.create())
    val result = updateMemberEmail.handle(UpdateMemberEmailCommand("authenticatedUser", "new email"))
    result should be("Email is invalid" failure)
  }

  "Change member information except his email" in {
    memberOrchestrator.handle(DummyAuthenticatedUser.create())
    updateMemberEmail.handle(UpdateMemberEmailCommand("authenticatedUser", "new.email@email.com"))
    val member = memberOrchestrator.handle(DummyAuthenticatedUser.create(firstName = "first", lastName = "name", email = "other.email@email.com", avatar = "new avatar", profileUrl = "new profile"))

    member should be(Member(
      id = "authenticatedUser",
      firstName = "first",
      lastName = "name",
      email = "new.email@email.com",
      avatar = "new avatar",
      profileUrl = "new profile"
    ))
  }

}
