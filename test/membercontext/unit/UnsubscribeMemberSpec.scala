package membercontext.unit

import commoncontext.unit.DummyAuthenticatedUser
import membercontext.adapters.gateways.inmemory.InMemoryMemberRepository
import membercontext.domain.commands.UnsubscribeMemberCommand
import membercontext.domain.gateways.CommandMemberRepository
import membercontext.usecases.commands.UnsubscribeMember
import membercontext.usecases.queries.MemberReadModel
import org.scalatest.BeforeAndAfterEach
import org.scalatest.Matchers._
import org.scalatestplus.play.PlaySpec
import scalaz.Scalaz._

class UnsubscribeMemberSpec extends PlaySpec with BeforeAndAfterEach {

  var memberRepository: InMemoryMemberRepository = _
  var commandMemberRepository: CommandMemberRepository = _
  var memberReadModel: MemberReadModel = _

  override protected def beforeEach(): Unit = {
    memberRepository = new InMemoryMemberRepository()
    commandMemberRepository = memberRepository
    memberReadModel = new MemberReadModel(memberRepository)
  }

  "Stock authorized user information" in {
    val unsubscribeMemberCommand: UnsubscribeMemberCommand = UnsubscribeMemberCommand(
      id = DummyAuthenticatedUser.create().id
    )

    val result = new UnsubscribeMember(commandMemberRepository).handle(unsubscribeMemberCommand)
    result should be(true success)

    memberRepository.removeWith should be(DummyAuthenticatedUser.create().id)
    memberReadModel.getMemberById(DummyAuthenticatedUser.create().id) should be(None)
  }

}
