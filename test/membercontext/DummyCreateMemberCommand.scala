package membercontext

import membercontext.domain.commands.CreateMemberCommand

object DummyCreateMemberCommand {

  def create(id: String = "id",
             firstName: String = "firstName",
             lastName: String = "lastName",
             email: String = "email",
             avatar: String = "avatar",
             profileUrl: String = "profile url"): CreateMemberCommand = CreateMemberCommand(
    id = id,
    firstName = firstName,
    lastName = lastName,
    email = email,
    avatar = avatar,
    profileUrl = profileUrl
  )

}
