package membercontext

import membercontext.domain.entities.Member

object DummyMember {

  def create(id: String = "id",
             firstName: String = "firstName",
             lastName: String = "lastName",
             email: String = "email",
             avatar: String = "avatar",
             profileUrl: String = "profile url"): Member = Member(
    id = id,
    firstName = firstName,
    lastName = lastName,
    email = email,
    avatar = avatar,
    profileUrl = profileUrl
  )

}
