$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("test/membercontext/acceptance/features/member_management..feature");
formatter.feature({
  "name": "Member management",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Member gets his information for the first time",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "a authorized user",
  "keyword": "Given "
});
formatter.step({
  "name": "member \u003cid\u003e wants to see his information",
  "keyword": "When "
});
formatter.step({
  "name": "member \u003cid\u003e is \u003cfirst_name\u003e - \u003clast_name\u003e, his email is \u003cemail\u003e, his avatar is \u003cavatar\u003e and his profile can be see at \u003cprofile_url\u003e",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "id",
        "first_name",
        "last_name",
        "email",
        "avatar",
        "profile_url"
      ]
    },
    {
      "cells": [
        "registeredUser",
        "first name",
        "last name",
        "email",
        "avatar",
        "profile url"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Member gets his information for the first time",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "a authorized user",
  "keyword": "Given "
});
formatter.match({
  "location": "MemberManagementSteps.scala:21"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "member registeredUser wants to see his information",
  "keyword": "When "
});
formatter.match({
  "location": "MemberManagementSteps.scala:29"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "member registeredUser is first name - last name, his email is email, his avatar is avatar and his profile can be see at profile url",
  "keyword": "Then "
});
formatter.match({
  "location": "MemberManagementSteps.scala:38"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Member gets his information after changing his email",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "a authorized user",
  "keyword": "Given ",
  "rows": [
    {
      "cells": [
        "id",
        "first_name",
        "last_name",
        "email",
        "avatar"
      ]
    },
    {
      "cells": [
        "registeredUser",
        "first name",
        "last name",
        "email",
        "avatar"
      ]
    }
  ]
});
formatter.step({
  "name": "member \u003cid\u003e changes his email for \u003cemail\u003e",
  "keyword": "When "
});
formatter.step({
  "name": "member \u003cid\u003e wants to see his information",
  "keyword": "When "
});
formatter.step({
  "name": "member \u003cid\u003e is \u003cfirst_name\u003e - \u003clast_name\u003e, his email is \u003cemail\u003e, his avatar is \u003cavatar\u003e and his profile can be see at \u003cprofile_url\u003e",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "id",
        "first_name",
        "last_name",
        "email",
        "avatar",
        "profile_url"
      ]
    },
    {
      "cells": [
        "registeredUser",
        "first name",
        "last name",
        "new.email@gmail.com",
        "avatar",
        "profile url"
      ]
    },
    {
      "cells": [
        "registeredUser",
        "first name",
        "last name",
        "email",
        "avatar",
        "profile url"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Member gets his information after changing his email",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "a authorized user",
  "rows": [
    {
      "cells": [
        "id",
        "first_name",
        "last_name",
        "email",
        "avatar"
      ]
    },
    {
      "cells": [
        "registeredUser",
        "first name",
        "last name",
        "email",
        "avatar"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "MemberManagementSteps.scala:21"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "member registeredUser changes his email for new.email@gmail.com",
  "keyword": "When "
});
formatter.match({
  "location": "MemberManagementSteps.scala:33"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "member registeredUser wants to see his information",
  "keyword": "When "
});
formatter.match({
  "location": "MemberManagementSteps.scala:29"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "member registeredUser is first name - last name, his email is new.email@gmail.com, his avatar is avatar and his profile can be see at profile url",
  "keyword": "Then "
});
formatter.match({
  "location": "MemberManagementSteps.scala:38"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Member gets his information after changing his email",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "a authorized user",
  "rows": [
    {
      "cells": [
        "id",
        "first_name",
        "last_name",
        "email",
        "avatar"
      ]
    },
    {
      "cells": [
        "registeredUser",
        "first name",
        "last name",
        "email",
        "avatar"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "MemberManagementSteps.scala:21"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "member registeredUser changes his email for email",
  "keyword": "When "
});
formatter.match({
  "location": "MemberManagementSteps.scala:33"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "member registeredUser wants to see his information",
  "keyword": "When "
});
formatter.match({
  "location": "MemberManagementSteps.scala:29"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "member registeredUser is first name - last name, his email is email, his avatar is avatar and his profile can be see at profile url",
  "keyword": "Then "
});
formatter.match({
  "location": "MemberManagementSteps.scala:38"
});
formatter.result({
  "status": "passed"
});
formatter.uri("test/missioncontext/acceptance/features/mission_contact.feature");
formatter.feature({
  "name": "Mission contact",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Inform the author when someone contact him for one of his missions",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "bob who is a member",
  "keyword": "Given "
});
formatter.step({
  "name": "bob contact for mission \u003cid\u003e he writes \u003cobject\u003e and \u003cmessage\u003e",
  "keyword": "When "
});
formatter.step({
  "name": "the author is notified with \u003cobject_sent\u003e and \u003cmessage_sent\u003e",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "id",
        "object",
        "message",
        "object_sent",
        "message_sent"
      ]
    },
    {
      "cells": [
        "a-b-c-d-2",
        "object",
        "lorem ispum",
        "object",
        "lorem ipsum"
      ]
    }
  ]
});
formatter.background({
  "name": "",
  "description": "",
  "keyword": "Background"
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "published missions",
  "rows": [
    {
      "cells": [
        "id",
        "created_at",
        "name",
        "description",
        "average_daily_rate",
        "customer",
        "location",
        "start_date",
        "duration_in_months",
        "remote_is_possible",
        "author_id",
        "contract_type"
      ]
    },
    {
      "cells": [
        "a-b-c-d-2",
        "2018-11-18T15:34:01.000+01:00",
        "mission2",
        "lorem ipsum",
        "123",
        "customer",
        "location",
        "2018-12-18T15:34:01.000+01:00",
        "1",
        "false",
        "1",
        "apprenticeship"
      ]
    },
    {
      "cells": [
        "a-b-c-d-1",
        "2018-11-18T15:34:00.000+01:00",
        "mission1",
        "lorem ipsum",
        "456",
        "",
        "location",
        "2018-12-18T15:34:00.000+01:00",
        "+12",
        "true",
        "12",
        "cdd"
      ]
    },
    {
      "cells": [
        "a-b-c-d-3",
        "2018-11-17T15:34:01.000+01:00",
        "mission3",
        "lorem ipsum",
        "235",
        "customer",
        "location",
        "2018-12-17T15:34:01.000+01:00",
        "4",
        "false",
        "4",
        "cdi"
      ]
    },
    {
      "cells": [
        "a-b-c-d-4",
        "2018-11-25T15:34:01.000+01:00",
        "mission4",
        "lorem ipsum",
        "987",
        "",
        "location",
        "2018-12-25T15:34:01.000+01:00",
        "-1",
        "true",
        "1",
        "freelance"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:26"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Inform the author when someone contact him for one of his missions",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "bob who is a member",
  "keyword": "Given "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "bob contact for mission a-b-c-d-2 he writes object and lorem ispum",
  "keyword": "When "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.step({
  "name": "the author is notified with object and lorem ipsum",
  "keyword": "Then "
});
formatter.match({});
formatter.result({
  "status": "undefined"
});
formatter.uri("test/missioncontext/acceptance/features/missions_management.feature");
formatter.feature({
  "name": "Mission management",
  "description": "",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "name": "Should view all update missions which are published and the details of each",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "published missions",
  "keyword": "Given ",
  "rows": [
    {
      "cells": [
        "id",
        "created_at",
        "name",
        "description",
        "average_daily_rate",
        "customer",
        "location",
        "start_date",
        "duration_in_months",
        "remote_is_possible",
        "author_id",
        "contract_type"
      ]
    },
    {
      "cells": [
        "a-b-c-d-2",
        "2018-11-18T15:34:01.000+01:00",
        "mission2",
        "lorem ipsum",
        "123",
        "customer",
        "location",
        "2018-12-18T15:34:01.000+01:00",
        "1",
        "false",
        "1",
        "apprenticeship"
      ]
    },
    {
      "cells": [
        "a-b-c-d-1",
        "2018-11-18T15:34:00.000+01:00",
        "mission1",
        "lorem ipsum",
        "456",
        "",
        "location",
        "2018-12-18T15:34:00.000+01:00",
        "+12",
        "true",
        "12",
        "cdd"
      ]
    },
    {
      "cells": [
        "a-b-c-d-3",
        "2018-11-17T15:34:01.000+01:00",
        "mission3",
        "lorem ipsum",
        "235",
        "customer",
        "location",
        "2018-12-17T15:34:01.000+01:00",
        "4",
        "false",
        "4",
        "cdi"
      ]
    },
    {
      "cells": [
        "a-b-c-d-4",
        "2018-11-25T15:34:01.000+01:00",
        "mission4",
        "lorem ipsum",
        "987",
        "",
        "location",
        "2018-12-25T15:34:01.000+01:00",
        "-1",
        "true",
        "1",
        "freelance"
      ]
    }
  ]
});
formatter.step({
  "name": "bob wants to see all missions strictly after 2018-12-18",
  "keyword": "When "
});
formatter.step({
  "name": "bob sees 3 missions",
  "keyword": "Then "
});
formatter.step({
  "name": "bob wants to see the details of the mission \u003cid\u003e",
  "keyword": "When "
});
formatter.step({
  "name": "bob sees missions from the most recent to the older one",
  "keyword": "Then ",
  "rows": [
    {
      "cells": [
        "id",
        "created_at",
        "name",
        "description",
        "average_daily_rate",
        "start_date",
        "author_id"
      ]
    },
    {
      "cells": [
        "a-b-c-d-4",
        "2018-11-25T15:34:01.000+01:00",
        "mission4",
        "lorem ipsum",
        "987",
        "2018-12-25T15:34:01.000+01:00",
        "1"
      ]
    },
    {
      "cells": [
        "a-b-c-d-2",
        "2018-11-18T15:34:01.000+01:00",
        "mission2",
        "lorem ipsum",
        "123",
        "2018-12-18T15:34:01.000+01:00",
        "1"
      ]
    },
    {
      "cells": [
        "a-b-c-d-1",
        "2018-11-18T15:34:00.000+01:00",
        "mission1",
        "lorem ipsum",
        "456",
        "2018-12-18T15:34:00.000+01:00",
        "12"
      ]
    }
  ]
});
formatter.step({
  "name": "bob sees the details of the mission \u003cid\u003e named \u003cname\u003e published by \u003cauthor_id\u003e which starts \u003cstart_date\u003e during \u003cduration_in_months\u003e months for \u003ccustomer\u003e at \u003clocation\u003e has a description \u003cdescription\u003e. The mission is payed \u003caverage_daily_rate\u003e, it\u0027s for \u003ccontract_type\u003e and the remote is \u003cremote_is_possible\u003e",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "id",
        "name",
        "description",
        "average_daily_rate",
        "customer",
        "location",
        "start_date",
        "duration_in_months",
        "remote_is_possible",
        "author_id",
        "contract_type"
      ]
    },
    {
      "cells": [
        "a-b-c-d-1",
        "mission1",
        "lorem ipsum",
        "456",
        "",
        "location",
        "2018-12-18T15:34:00.000+01:00",
        "+12",
        "true",
        "12",
        "cdd"
      ]
    },
    {
      "cells": [
        "a-b-c-d-2",
        "mission2",
        "lorem ipsum",
        "123",
        "customer",
        "location",
        "2018-12-18T15:34:01.000+01:00",
        "1",
        "false",
        "1",
        "apprenticeship"
      ]
    },
    {
      "cells": [
        "a-b-c-d-3",
        "mission3",
        "lorem ipsum",
        "235",
        "customer",
        "location",
        "2018-12-17T15:34:01.000+01:00",
        "4",
        "false",
        "4",
        "cdi"
      ]
    },
    {
      "cells": [
        "a-b-c-d-4",
        "mission4",
        "lorem ipsum",
        "987",
        "",
        "location",
        "2018-12-25T15:34:01.000+01:00",
        "-1",
        "true",
        "1",
        "freelance"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Should view all update missions which are published and the details of each",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "published missions",
  "rows": [
    {
      "cells": [
        "id",
        "created_at",
        "name",
        "description",
        "average_daily_rate",
        "customer",
        "location",
        "start_date",
        "duration_in_months",
        "remote_is_possible",
        "author_id",
        "contract_type"
      ]
    },
    {
      "cells": [
        "a-b-c-d-2",
        "2018-11-18T15:34:01.000+01:00",
        "mission2",
        "lorem ipsum",
        "123",
        "customer",
        "location",
        "2018-12-18T15:34:01.000+01:00",
        "1",
        "false",
        "1",
        "apprenticeship"
      ]
    },
    {
      "cells": [
        "a-b-c-d-1",
        "2018-11-18T15:34:00.000+01:00",
        "mission1",
        "lorem ipsum",
        "456",
        "",
        "location",
        "2018-12-18T15:34:00.000+01:00",
        "+12",
        "true",
        "12",
        "cdd"
      ]
    },
    {
      "cells": [
        "a-b-c-d-3",
        "2018-11-17T15:34:01.000+01:00",
        "mission3",
        "lorem ipsum",
        "235",
        "customer",
        "location",
        "2018-12-17T15:34:01.000+01:00",
        "4",
        "false",
        "4",
        "cdi"
      ]
    },
    {
      "cells": [
        "a-b-c-d-4",
        "2018-11-25T15:34:01.000+01:00",
        "mission4",
        "lorem ipsum",
        "987",
        "",
        "location",
        "2018-12-25T15:34:01.000+01:00",
        "-1",
        "true",
        "1",
        "freelance"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:26"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob wants to see all missions strictly after 2018-12-18",
  "keyword": "When "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:48"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob sees 3 missions",
  "keyword": "Then "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:82"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob wants to see the details of the mission a-b-c-d-1",
  "keyword": "When "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:52"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob sees missions from the most recent to the older one",
  "rows": [
    {
      "cells": [
        "id",
        "created_at",
        "name",
        "description",
        "average_daily_rate",
        "start_date",
        "author_id"
      ]
    },
    {
      "cells": [
        "a-b-c-d-4",
        "2018-11-25T15:34:01.000+01:00",
        "mission4",
        "lorem ipsum",
        "987",
        "2018-12-25T15:34:01.000+01:00",
        "1"
      ]
    },
    {
      "cells": [
        "a-b-c-d-2",
        "2018-11-18T15:34:01.000+01:00",
        "mission2",
        "lorem ipsum",
        "123",
        "2018-12-18T15:34:01.000+01:00",
        "1"
      ]
    },
    {
      "cells": [
        "a-b-c-d-1",
        "2018-11-18T15:34:00.000+01:00",
        "mission1",
        "lorem ipsum",
        "456",
        "2018-12-18T15:34:00.000+01:00",
        "12"
      ]
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:86"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob sees the details of the mission a-b-c-d-1 named mission1 published by 12 which starts 2018-12-18T15:34:00.000+01:00 during +12 months for  at location has a description lorem ipsum. The mission is payed 456, it\u0027s for cdd and the remote is true",
  "keyword": "Then "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:105"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Should view all update missions which are published and the details of each",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "published missions",
  "rows": [
    {
      "cells": [
        "id",
        "created_at",
        "name",
        "description",
        "average_daily_rate",
        "customer",
        "location",
        "start_date",
        "duration_in_months",
        "remote_is_possible",
        "author_id",
        "contract_type"
      ]
    },
    {
      "cells": [
        "a-b-c-d-2",
        "2018-11-18T15:34:01.000+01:00",
        "mission2",
        "lorem ipsum",
        "123",
        "customer",
        "location",
        "2018-12-18T15:34:01.000+01:00",
        "1",
        "false",
        "1",
        "apprenticeship"
      ]
    },
    {
      "cells": [
        "a-b-c-d-1",
        "2018-11-18T15:34:00.000+01:00",
        "mission1",
        "lorem ipsum",
        "456",
        "",
        "location",
        "2018-12-18T15:34:00.000+01:00",
        "+12",
        "true",
        "12",
        "cdd"
      ]
    },
    {
      "cells": [
        "a-b-c-d-3",
        "2018-11-17T15:34:01.000+01:00",
        "mission3",
        "lorem ipsum",
        "235",
        "customer",
        "location",
        "2018-12-17T15:34:01.000+01:00",
        "4",
        "false",
        "4",
        "cdi"
      ]
    },
    {
      "cells": [
        "a-b-c-d-4",
        "2018-11-25T15:34:01.000+01:00",
        "mission4",
        "lorem ipsum",
        "987",
        "",
        "location",
        "2018-12-25T15:34:01.000+01:00",
        "-1",
        "true",
        "1",
        "freelance"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:26"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob wants to see all missions strictly after 2018-12-18",
  "keyword": "When "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:48"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob sees 3 missions",
  "keyword": "Then "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:82"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob wants to see the details of the mission a-b-c-d-2",
  "keyword": "When "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:52"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob sees missions from the most recent to the older one",
  "rows": [
    {
      "cells": [
        "id",
        "created_at",
        "name",
        "description",
        "average_daily_rate",
        "start_date",
        "author_id"
      ]
    },
    {
      "cells": [
        "a-b-c-d-4",
        "2018-11-25T15:34:01.000+01:00",
        "mission4",
        "lorem ipsum",
        "987",
        "2018-12-25T15:34:01.000+01:00",
        "1"
      ]
    },
    {
      "cells": [
        "a-b-c-d-2",
        "2018-11-18T15:34:01.000+01:00",
        "mission2",
        "lorem ipsum",
        "123",
        "2018-12-18T15:34:01.000+01:00",
        "1"
      ]
    },
    {
      "cells": [
        "a-b-c-d-1",
        "2018-11-18T15:34:00.000+01:00",
        "mission1",
        "lorem ipsum",
        "456",
        "2018-12-18T15:34:00.000+01:00",
        "12"
      ]
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:86"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob sees the details of the mission a-b-c-d-2 named mission2 published by 1 which starts 2018-12-18T15:34:01.000+01:00 during 1 months for customer at location has a description lorem ipsum. The mission is payed 123, it\u0027s for apprenticeship and the remote is false",
  "keyword": "Then "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:105"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Should view all update missions which are published and the details of each",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "published missions",
  "rows": [
    {
      "cells": [
        "id",
        "created_at",
        "name",
        "description",
        "average_daily_rate",
        "customer",
        "location",
        "start_date",
        "duration_in_months",
        "remote_is_possible",
        "author_id",
        "contract_type"
      ]
    },
    {
      "cells": [
        "a-b-c-d-2",
        "2018-11-18T15:34:01.000+01:00",
        "mission2",
        "lorem ipsum",
        "123",
        "customer",
        "location",
        "2018-12-18T15:34:01.000+01:00",
        "1",
        "false",
        "1",
        "apprenticeship"
      ]
    },
    {
      "cells": [
        "a-b-c-d-1",
        "2018-11-18T15:34:00.000+01:00",
        "mission1",
        "lorem ipsum",
        "456",
        "",
        "location",
        "2018-12-18T15:34:00.000+01:00",
        "+12",
        "true",
        "12",
        "cdd"
      ]
    },
    {
      "cells": [
        "a-b-c-d-3",
        "2018-11-17T15:34:01.000+01:00",
        "mission3",
        "lorem ipsum",
        "235",
        "customer",
        "location",
        "2018-12-17T15:34:01.000+01:00",
        "4",
        "false",
        "4",
        "cdi"
      ]
    },
    {
      "cells": [
        "a-b-c-d-4",
        "2018-11-25T15:34:01.000+01:00",
        "mission4",
        "lorem ipsum",
        "987",
        "",
        "location",
        "2018-12-25T15:34:01.000+01:00",
        "-1",
        "true",
        "1",
        "freelance"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:26"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob wants to see all missions strictly after 2018-12-18",
  "keyword": "When "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:48"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob sees 3 missions",
  "keyword": "Then "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:82"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob wants to see the details of the mission a-b-c-d-3",
  "keyword": "When "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:52"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob sees missions from the most recent to the older one",
  "rows": [
    {
      "cells": [
        "id",
        "created_at",
        "name",
        "description",
        "average_daily_rate",
        "start_date",
        "author_id"
      ]
    },
    {
      "cells": [
        "a-b-c-d-4",
        "2018-11-25T15:34:01.000+01:00",
        "mission4",
        "lorem ipsum",
        "987",
        "2018-12-25T15:34:01.000+01:00",
        "1"
      ]
    },
    {
      "cells": [
        "a-b-c-d-2",
        "2018-11-18T15:34:01.000+01:00",
        "mission2",
        "lorem ipsum",
        "123",
        "2018-12-18T15:34:01.000+01:00",
        "1"
      ]
    },
    {
      "cells": [
        "a-b-c-d-1",
        "2018-11-18T15:34:00.000+01:00",
        "mission1",
        "lorem ipsum",
        "456",
        "2018-12-18T15:34:00.000+01:00",
        "12"
      ]
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:86"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob sees the details of the mission a-b-c-d-3 named mission3 published by 4 which starts 2018-12-17T15:34:01.000+01:00 during 4 months for customer at location has a description lorem ipsum. The mission is payed 235, it\u0027s for cdi and the remote is false",
  "keyword": "Then "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:105"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Should view all update missions which are published and the details of each",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "published missions",
  "rows": [
    {
      "cells": [
        "id",
        "created_at",
        "name",
        "description",
        "average_daily_rate",
        "customer",
        "location",
        "start_date",
        "duration_in_months",
        "remote_is_possible",
        "author_id",
        "contract_type"
      ]
    },
    {
      "cells": [
        "a-b-c-d-2",
        "2018-11-18T15:34:01.000+01:00",
        "mission2",
        "lorem ipsum",
        "123",
        "customer",
        "location",
        "2018-12-18T15:34:01.000+01:00",
        "1",
        "false",
        "1",
        "apprenticeship"
      ]
    },
    {
      "cells": [
        "a-b-c-d-1",
        "2018-11-18T15:34:00.000+01:00",
        "mission1",
        "lorem ipsum",
        "456",
        "",
        "location",
        "2018-12-18T15:34:00.000+01:00",
        "+12",
        "true",
        "12",
        "cdd"
      ]
    },
    {
      "cells": [
        "a-b-c-d-3",
        "2018-11-17T15:34:01.000+01:00",
        "mission3",
        "lorem ipsum",
        "235",
        "customer",
        "location",
        "2018-12-17T15:34:01.000+01:00",
        "4",
        "false",
        "4",
        "cdi"
      ]
    },
    {
      "cells": [
        "a-b-c-d-4",
        "2018-11-25T15:34:01.000+01:00",
        "mission4",
        "lorem ipsum",
        "987",
        "",
        "location",
        "2018-12-25T15:34:01.000+01:00",
        "-1",
        "true",
        "1",
        "freelance"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:26"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob wants to see all missions strictly after 2018-12-18",
  "keyword": "When "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:48"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob sees 3 missions",
  "keyword": "Then "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:82"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob wants to see the details of the mission a-b-c-d-4",
  "keyword": "When "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:52"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob sees missions from the most recent to the older one",
  "rows": [
    {
      "cells": [
        "id",
        "created_at",
        "name",
        "description",
        "average_daily_rate",
        "start_date",
        "author_id"
      ]
    },
    {
      "cells": [
        "a-b-c-d-4",
        "2018-11-25T15:34:01.000+01:00",
        "mission4",
        "lorem ipsum",
        "987",
        "2018-12-25T15:34:01.000+01:00",
        "1"
      ]
    },
    {
      "cells": [
        "a-b-c-d-2",
        "2018-11-18T15:34:01.000+01:00",
        "mission2",
        "lorem ipsum",
        "123",
        "2018-12-18T15:34:01.000+01:00",
        "1"
      ]
    },
    {
      "cells": [
        "a-b-c-d-1",
        "2018-11-18T15:34:00.000+01:00",
        "mission1",
        "lorem ipsum",
        "456",
        "2018-12-18T15:34:00.000+01:00",
        "12"
      ]
    }
  ],
  "keyword": "Then "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:86"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob sees the details of the mission a-b-c-d-4 named mission4 published by 1 which starts 2018-12-25T15:34:01.000+01:00 during -1 months for  at location has a description lorem ipsum. The mission is payed 987, it\u0027s for freelance and the remote is true",
  "keyword": "Then "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:105"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Should publish a mission",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "published missions",
  "keyword": "Given ",
  "rows": [
    {
      "cells": [
        "id",
        "created_at",
        "name",
        "description",
        "average_daily_rate",
        "customer",
        "location",
        "start_date",
        "duration_in_months",
        "remote_is_possible",
        "author_id",
        "contract_type"
      ]
    },
    {
      "cells": [
        "a-b-c-d-2",
        "2018-11-18T15:34:01.000+01:00",
        "mission2",
        "lorem ipsum",
        "123",
        "customer",
        "location",
        "2018-12-18T15:34:01.000+01:00",
        "1",
        "false",
        "1",
        "interim"
      ]
    }
  ]
});
formatter.step({
  "name": "bob publishes the mission \u003cid\u003e",
  "keyword": "When "
});
formatter.step({
  "name": "bob wants to see all missions strictly after 2018-11-18",
  "keyword": "When "
});
formatter.step({
  "name": "bob sees \u003ctotal_missions\u003e missions",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "id",
        "total_missions"
      ]
    },
    {
      "cells": [
        "a-b-c-d-1",
        "2"
      ]
    },
    {
      "cells": [
        "a-b-c-d-2",
        "1"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Should publish a mission",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "published missions",
  "rows": [
    {
      "cells": [
        "id",
        "created_at",
        "name",
        "description",
        "average_daily_rate",
        "customer",
        "location",
        "start_date",
        "duration_in_months",
        "remote_is_possible",
        "author_id",
        "contract_type"
      ]
    },
    {
      "cells": [
        "a-b-c-d-2",
        "2018-11-18T15:34:01.000+01:00",
        "mission2",
        "lorem ipsum",
        "123",
        "customer",
        "location",
        "2018-12-18T15:34:01.000+01:00",
        "1",
        "false",
        "1",
        "interim"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:26"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob publishes the mission a-b-c-d-1",
  "keyword": "When "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:56"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob wants to see all missions strictly after 2018-11-18",
  "keyword": "When "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:48"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob sees 2 missions",
  "keyword": "Then "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:82"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Should publish a mission",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "published missions",
  "rows": [
    {
      "cells": [
        "id",
        "created_at",
        "name",
        "description",
        "average_daily_rate",
        "customer",
        "location",
        "start_date",
        "duration_in_months",
        "remote_is_possible",
        "author_id",
        "contract_type"
      ]
    },
    {
      "cells": [
        "a-b-c-d-2",
        "2018-11-18T15:34:01.000+01:00",
        "mission2",
        "lorem ipsum",
        "123",
        "customer",
        "location",
        "2018-12-18T15:34:01.000+01:00",
        "1",
        "false",
        "1",
        "interim"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:26"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob publishes the mission a-b-c-d-2",
  "keyword": "When "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:56"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob wants to see all missions strictly after 2018-11-18",
  "keyword": "When "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:48"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob sees 1 missions",
  "keyword": "Then "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:82"
});
formatter.result({
  "status": "passed"
});
formatter.scenarioOutline({
  "name": "Should remove only member\u0027s mission",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.step({
  "name": "published missions",
  "keyword": "Given ",
  "rows": [
    {
      "cells": [
        "id",
        "created_at",
        "name",
        "description",
        "average_daily_rate",
        "customer",
        "location",
        "start_date",
        "duration_in_months",
        "remote_is_possible",
        "author_id",
        "contract_type"
      ]
    },
    {
      "cells": [
        "a-b-c-d-1",
        "2018-11-18T15:34:01.000+01:00",
        "mission2",
        "lorem ipsum",
        "123",
        "customer",
        "location",
        "2018-12-18T15:34:01.000+01:00",
        "1",
        "false",
        "1",
        "portage"
      ]
    },
    {
      "cells": [
        "a-b-c-d-2",
        "2018-11-18T15:34:00.000+01:00",
        "mission1",
        "lorem ipsum",
        "456",
        "",
        "location",
        "2018-12-18T15:34:00.000+01:00",
        "+12",
        "true",
        "12",
        "other"
      ]
    }
  ]
});
formatter.step({
  "name": "bob removes the mission \u003cid\u003e of \u003cauthor_id\u003e",
  "keyword": "When "
});
formatter.step({
  "name": "bob wants to see all missions strictly after 2018-11-18",
  "keyword": "When "
});
formatter.step({
  "name": "bob sees \u003cnb_missions\u003e missions",
  "keyword": "Then "
});
formatter.examples({
  "name": "",
  "description": "",
  "keyword": "Examples",
  "rows": [
    {
      "cells": [
        "id",
        "author_id",
        "nb_missions"
      ]
    },
    {
      "cells": [
        "a-b-c-d-1",
        "1",
        "1"
      ]
    },
    {
      "cells": [
        "a-b-c-d-2",
        "12",
        "2"
      ]
    }
  ]
});
formatter.scenario({
  "name": "Should remove only member\u0027s mission",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "published missions",
  "rows": [
    {
      "cells": [
        "id",
        "created_at",
        "name",
        "description",
        "average_daily_rate",
        "customer",
        "location",
        "start_date",
        "duration_in_months",
        "remote_is_possible",
        "author_id",
        "contract_type"
      ]
    },
    {
      "cells": [
        "a-b-c-d-1",
        "2018-11-18T15:34:01.000+01:00",
        "mission2",
        "lorem ipsum",
        "123",
        "customer",
        "location",
        "2018-12-18T15:34:01.000+01:00",
        "1",
        "false",
        "1",
        "portage"
      ]
    },
    {
      "cells": [
        "a-b-c-d-2",
        "2018-11-18T15:34:00.000+01:00",
        "mission1",
        "lorem ipsum",
        "456",
        "",
        "location",
        "2018-12-18T15:34:00.000+01:00",
        "+12",
        "true",
        "12",
        "other"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:26"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob removes the mission a-b-c-d-1 of 1",
  "keyword": "When "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:73"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob wants to see all missions strictly after 2018-11-18",
  "keyword": "When "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:48"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob sees 1 missions",
  "keyword": "Then "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:82"
});
formatter.result({
  "status": "passed"
});
formatter.scenario({
  "name": "Should remove only member\u0027s mission",
  "description": "",
  "keyword": "Scenario Outline"
});
formatter.before({
  "status": "passed"
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "published missions",
  "rows": [
    {
      "cells": [
        "id",
        "created_at",
        "name",
        "description",
        "average_daily_rate",
        "customer",
        "location",
        "start_date",
        "duration_in_months",
        "remote_is_possible",
        "author_id",
        "contract_type"
      ]
    },
    {
      "cells": [
        "a-b-c-d-1",
        "2018-11-18T15:34:01.000+01:00",
        "mission2",
        "lorem ipsum",
        "123",
        "customer",
        "location",
        "2018-12-18T15:34:01.000+01:00",
        "1",
        "false",
        "1",
        "portage"
      ]
    },
    {
      "cells": [
        "a-b-c-d-2",
        "2018-11-18T15:34:00.000+01:00",
        "mission1",
        "lorem ipsum",
        "456",
        "",
        "location",
        "2018-12-18T15:34:00.000+01:00",
        "+12",
        "true",
        "12",
        "other"
      ]
    }
  ],
  "keyword": "Given "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:26"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob removes the mission a-b-c-d-2 of 12",
  "keyword": "When "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:73"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob wants to see all missions strictly after 2018-11-18",
  "keyword": "When "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:48"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "bob sees 2 missions",
  "keyword": "Then "
});
formatter.match({
  "location": "MissionsManagementSteps.scala:82"
});
formatter.result({
  "status": "passed"
});
});