logLevel := Level.Warn

resolvers += "Typesafe repository" at "http://repo.typesafe.com/typesafe/releases/"

addSbtPlugin("au.com.onegeek" % "sbt-dotenv" % "2.0.109")
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.6.20")
