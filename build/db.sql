create table member
(
  id VARCHAR(255) not null,
  first_name VARCHAR(255) not null,
  last_name VARCHAR(255) not null,
  email VARCHAR(255) not null,
  avatar VARCHAR(255) not null,
  profile_url VARCHAR(255) not null
);

create unique index member_id_uindex on member (id);
alter table member add constraint member_pk primary key (id);

create table mission
(
  uuid varchar(255) not null,
  name varchar(255) not null,
  description TEXT not null,
  average_daily_rate int not null,
  customer varchar(255),
  location varchar(255) not null,
  start_date varchar(255) not null,
  duration_in_months varchar(255) not null,
  remote_is_possible boolean not null,
  author_id varchar(255) not null,
  contract_type int not null,
  created_at varchar(255) not null default NOW()
);

create unique index mission_uuid_uindex on mission (uuid);
alter table mission add constraint mission_pk primary key (uuid);

create table contract_type
(
  id serial not null,
  name varchar(255) not null
);

create unique index contract_type_id_uindex on contract_type (id);
alter table contract_type add constraint contract_type_pk primary key (id);
alter table mission add constraint mission_contract_type_id_fk foreign key (contract_type) references contract_type;
alter table mission add constraint mission_member_id_fk foreign key (author_id) references member on delete cascade;

INSERT INTO public.contract_type (id, name) VALUES (1, 'APPRENTICESHIP');
INSERT INTO public.contract_type (id, name) VALUES (2, 'CDD');
INSERT INTO public.contract_type (id, name) VALUES (3, 'CDI');
INSERT INTO public.contract_type (id, name) VALUES (4, 'FREELANCE');
INSERT INTO public.contract_type (id, name) VALUES (5, 'INTERN');
INSERT INTO public.contract_type (id, name) VALUES (6, 'PORTAGE');
INSERT INTO public.contract_type (id, name) VALUES (7, 'OTHER');

create table mission_contact
(
  id serial not null
    constraint mission_contact_pk
      primary key,
  mission_id varchar(255) not null,
  author_id varchar(255) not null,
  subject varchar(255) not null,
  message text not null,
  sender_email varchar(255) not null,
  recipient_email varchar(255) not null,
  sent_at varchar(255) default current_date not null
);

alter table mission_contact add constraint mission_contact_member_id_fk foreign key (author_id) references member on delete cascade;

alter table mission_contact add constraint mission_contact_mission_uuid_fk foreign key (mission_id) references mission on delete cascade;
