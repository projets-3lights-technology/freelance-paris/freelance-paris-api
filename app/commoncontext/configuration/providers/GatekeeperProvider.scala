package commoncontext.configuration.providers

import com.google.inject.{Inject, Provider}
import commoncontext.domain.gateways.IdentifierChecker
import commoncontext.usecases.Gatekeeper

class GatekeeperProvider @Inject()(identifierChecker: IdentifierChecker)
  extends Provider[Gatekeeper] {

  def get(): Gatekeeper = new Gatekeeper(identifierChecker)

}
