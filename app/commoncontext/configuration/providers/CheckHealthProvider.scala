package commoncontext.configuration.providers

import com.google.inject.{Inject, Provider}
import commoncontext.domain.gateways.HealthChecker
import commoncontext.usecases.CheckHealth

class CheckHealthProvider @Inject()(healthChecker: HealthChecker)
  extends Provider[CheckHealth] {

  def get(): CheckHealth = new CheckHealth(healthChecker)

}
