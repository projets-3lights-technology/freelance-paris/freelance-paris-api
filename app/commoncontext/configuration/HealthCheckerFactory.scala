package commoncontext.configuration

import commoncontext.adapters.gateways.inmemory.InMemoryHealthChecker
import commoncontext.adapters.gateways.real.PostgresHealthChecker
import commoncontext.domain.gateways.HealthChecker
import configuration.environment.AppConfiguration
import configuration.environment.ConfigurationKey._

case class HealthCheckerFactory(appConfiguration: AppConfiguration) {

  def makeHealthChecker: HealthChecker =
    appConfiguration.get(MODE) match {
      case Some("inmemory") => new InMemoryHealthChecker()
      case _ => new PostgresHealthChecker(appConfiguration)
    }

}
