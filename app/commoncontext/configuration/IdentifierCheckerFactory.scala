package commoncontext.configuration

import commoncontext.adapters.gateways.inmemory.InMemoryIdentifierChecker
import commoncontext.adapters.gateways.real.LinkedInIdentifierChecker
import commoncontext.domain.gateways.IdentifierChecker
import configuration.environment.AppConfiguration
import configuration.environment.ConfigurationKey._

case class IdentifierCheckerFactory(appConfiguration: AppConfiguration) {

  def makeIdentifierChecker: Class[_ <: IdentifierChecker] =
    appConfiguration.get(MODE) match {
      case Some("inmemory") => classOf[InMemoryIdentifierChecker]
      case _ => classOf[LinkedInIdentifierChecker]
    }

}
