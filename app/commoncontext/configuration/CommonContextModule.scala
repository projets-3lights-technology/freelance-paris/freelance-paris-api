package commoncontext.configuration

import com.google.inject.{AbstractModule, TypeLiteral}
import commoncontext.configuration.providers.{CheckHealthProvider, GatekeeperProvider}
import commoncontext.domain.gateways.{HealthChecker, IdentifierChecker}
import commoncontext.usecases.{CheckHealth, Gatekeeper}
import configuration.environment.EnvVarAppConfiguration

class CommonContextModule extends AbstractModule {

  override def configure(): Unit = {
    bind(new TypeLiteral[IdentifierChecker] {}) to IdentifierCheckerFactory(EnvVarAppConfiguration).makeIdentifierChecker
    bind(new TypeLiteral[HealthChecker] {}) toInstance HealthCheckerFactory(EnvVarAppConfiguration).makeHealthChecker

    bind(classOf[Gatekeeper]) toProvider classOf[GatekeeperProvider] asEagerSingleton()
    bind(classOf[CheckHealth]) toProvider classOf[CheckHealthProvider] asEagerSingleton()
  }

}
