package commoncontext.adapters.controllers.FormMapping

import play.api.data.Forms.of
import play.api.data.{FormError, Mapping}
import play.api.data.format.{Formats, Formatter}

object EnumMapping {

  def enum[E <: Enumeration](enum: E): Mapping[E#Value] = of(enumFormat(enum))

  private def enumFormat[E <: Enumeration](enum: E): Formatter[E#Value] = new Formatter[E#Value] {
    def bind(key: String, data: Map[String, String]): Either[Seq[FormError], E#Value] = {
      Formats.stringFormat.bind(key, data).right.flatMap { s =>
        scala.util.control.Exception.allCatch[E#Value]
          .either(enum.withName(s))
          .left.map(e => Seq(FormError(key, "error.enum ".concat(enum.values.mkString(", ")), Nil)))
      }
    }

    def unbind(key: String, value: E#Value): Map[String, String] = Map(key -> value.toString)
  }

}
