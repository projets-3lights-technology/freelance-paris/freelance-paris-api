package commoncontext.adapters.controllers.viewmodels

case class ErrorVM(error: String, error_description: String)
