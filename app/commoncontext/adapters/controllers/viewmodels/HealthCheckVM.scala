package commoncontext.adapters.controllers.viewmodels

case class HealthCheckVM(status: String,
                         data_base_status: String,
                         version: String)
