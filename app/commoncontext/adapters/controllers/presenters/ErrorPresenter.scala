package commoncontext.adapters.controllers.presenters

import commoncontext.adapters.controllers.viewmodels.ErrorVM
import play.api.data.Form

object ErrorPresenter {

  // todo: test
  def to400ErrorVM(formError: Form[_]): ErrorVM = {
    def convertToReadableError(error: String): String = error match {
      case "error.required" => "is required"
      case "error.number" => "must be a number"
      case "error.uuid" => "must be an UUID"
      case "error.email" => "must be have a valid email format"
      case "error.date" => "must be in ISO 8601 (e.g.: 2018-01-31T08:08:08.456+01:00)"
      case _ if error.contains("error.enum") => error.replace("error.enum", "must be a value of:")
      case _ => error
    }

    val description = formError.errors
      .map(error => error.key + " " + convertToReadableError(error.message))
      .mkString(" - ")
    ErrorVM("Bad Request", description)
  }

  def to400ErrorVM(error: String): ErrorVM = ErrorVM("Bad Request", error)

  def to409ErrorVM(error: String): ErrorVM = ErrorVM("Conflict", error)

}
