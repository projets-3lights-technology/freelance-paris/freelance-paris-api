package commoncontext.adapters.controllers

import commoncontext.adapters.controllers.viewmodels.HealthCheckVM
import commoncontext.usecases.CheckHealth
import javax.inject.Inject
import play.api.libs.json._
import play.api.mvc.{AbstractController, ControllerComponents}

class HealthCheck @Inject()(cc: ControllerComponents,
                            checkHealth: CheckHealth) extends AbstractController(cc) {

  private implicit val write: OWrites[HealthCheckVM] = Json.writes[HealthCheckVM]

  def check = Action {
    Ok(Json.toJson(HealthCheckVM(
      status = "UP",
      data_base_status = checkHealth.check,
      version = "v1"
    )))
  }

}
