package commoncontext.adapters.gateways.real

import commoncontext.domain.gateways.HealthChecker
import configuration.environment.AppConfiguration
import doobie.implicits._

class PostgresHealthChecker(appConfiguration: AppConfiguration)
  extends PostgresDataBaseConnection(appConfiguration)
    with HealthChecker {

  override def check: Boolean = {
    val request =
      sql"""
           | SELECT id FROM member LIMIT 1
      """.stripMargin

    queryOne(request.query[String], Some(false))(_ => true).get
  }

}
