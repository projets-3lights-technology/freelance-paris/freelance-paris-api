package commoncontext.adapters.gateways.real

import com.google.inject.Inject
import commoncontext.adapters.gateways.real.DTOs.LinkedInPeopleDTO
import commoncontext.adapters.gateways.real.mappers.AuthenticatedUserMapper._
import commoncontext.domain.entities.AuthenticatedUser
import commoncontext.domain.gateways.IdentifierChecker
import configuration.environment.AppConfiguration
import configuration.environment.ConfigurationKey._
import play.api.libs.json.{Json, Reads}
import play.api.libs.ws._

import scala.concurrent.Await
import scala.concurrent.duration.Duration

class LinkedInIdentifierChecker @Inject()(ws: WSClient,
                                          appConfiguration: AppConfiguration) extends IdentifierChecker {

  private implicit val personReads: Reads[LinkedInPeopleDTO] = Json.reads[LinkedInPeopleDTO]

  override def check(identifier: String): Option[AuthenticatedUser] = {
    val url = appConfiguration.get(LINKEDIN_URL).get.concat("people/~:(id,first-name,last-name,email-address,picture-url,public-profile-url)")
    val request = ws
      .url(url)
      .addHttpHeaders("x-li-format" -> "json")
      .addHttpHeaders("Content-Type" -> "application/json")
      .addHttpHeaders("Authorization" -> "Bearer ".concat(identifier))
      .get()

    val response = Await.result(request, Duration(3, "seconds"))
    response.status match {
      case 200 => Some(response.json.validate.map(r => r).get)
      case _ => None
    }
  }

}
