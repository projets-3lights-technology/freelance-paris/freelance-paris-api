package commoncontext.adapters.gateways.real.DTOs

case class LinkedInPeopleDTO(id: String,
                             firstName: String,
                             lastName: String,
                             emailAddress: String,
                             pictureUrl: String,
                             publicProfileUrl: String)
