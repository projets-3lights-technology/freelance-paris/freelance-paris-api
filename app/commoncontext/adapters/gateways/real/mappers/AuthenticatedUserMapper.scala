package commoncontext.adapters.gateways.real.mappers

import commoncontext.adapters.gateways.real.DTOs.LinkedInPeopleDTO
import commoncontext.domain.entities.AuthenticatedUser

object AuthenticatedUserMapper {

  implicit def toAuthenticatedUser(linkedInPeople: LinkedInPeopleDTO): AuthenticatedUser = AuthenticatedUser(
    id = linkedInPeople.id,
    firstName = linkedInPeople.firstName,
    lastName = linkedInPeople.lastName,
    email = linkedInPeople.emailAddress,
    avatar = linkedInPeople.pictureUrl,
    profileUrl = linkedInPeople.publicProfileUrl
  )

}
