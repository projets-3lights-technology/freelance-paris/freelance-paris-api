package commoncontext.adapters.gateways.real

import java.sql.SQLException

import cats.effect._
import configuration.environment.AppConfiguration
import configuration.environment.ConfigurationKey._
import doobie.Transactor
import doobie.implicits._
import doobie._
import doobie.util.transactor.Transactor.Aux
import scalaz.Scalaz._
import scalaz.Validation

import scala.concurrent.ExecutionContext
import scala.util.control.Exception.catching

class PostgresDataBaseConnection(appConfiguration: AppConfiguration) {

  protected val postgresDriver: String = appConfiguration.get(POSTGRES_DRIVER).get
  protected val postgresUrl: String = appConfiguration.get(POSTGRES_URL).get
  protected val postgresUser: String = appConfiguration.get(POSTGRES_USERNAME).get
  protected val postgresPassword: String = appConfiguration.get(POSTGRES_PASSWORD).get

  protected implicit val cs: ContextShift[IO] = IO.contextShift(ExecutionContext.global)

  protected val xa: Aux[IO, Unit] = Transactor.fromDriverManager[IO](
    postgresDriver,
    postgresUrl,
    postgresUser,
    postgresPassword
  )

  protected def queryList[A, B](query: Query0[A], default: B)(implicit f: List[A] => B): B =
    catching(classOf[SQLException]) either {
      query
        .stream
        .compile.toList
        .transact(xa)
        .unsafeRunSync()
    } match {
      case Left(_: Throwable) => default
      case Right(r) => f(r)
    }

  protected def queryOne[A, B](query: Query0[A], default: Option[B])(implicit f: A => B): Option[B] =
    catching(classOf[SQLException]) either {
      query
        .option
        .transact(xa)
        .unsafeRunSync()
    } match {
      case Left(_: Throwable) => default
      case Right(None) => default
      case Right(Some(r)) => Some(f(r))
    }

  protected def insert(query: Fragment): Validation[String, Boolean] = update(query)

  protected def delete(query: Fragment): Validation[String, Boolean] = update(query)

  protected def update(query: Fragment): Validation[String, Boolean] =
    catching(classOf[SQLException]) either {
      query
        .update
        .run
        .transact(xa)
        .unsafeRunSync()
    } match {
      case Left(e: Throwable) => e.toString failure
      case Right(_) => true success
    }

}
