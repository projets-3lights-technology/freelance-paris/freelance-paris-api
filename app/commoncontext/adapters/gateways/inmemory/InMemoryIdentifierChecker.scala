package commoncontext.adapters.gateways.inmemory

import commoncontext.domain.entities.AuthenticatedUser
import commoncontext.domain.gateways.IdentifierChecker

class InMemoryIdentifierChecker extends IdentifierChecker {

  override def check(identifier: String): Option[AuthenticatedUser] = identifier match {
    case "token" => Some(AuthenticatedUser(
      "registeredUser",
      "first name",
      "last name",
      "email",
      "avatar",
      "profile url"
    ))
    case _ => None
  }

}
