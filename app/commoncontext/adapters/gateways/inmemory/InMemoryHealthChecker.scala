package commoncontext.adapters.gateways.inmemory

import commoncontext.domain.gateways.HealthChecker
import scalaz.Scalaz._

class InMemoryHealthChecker(status: String = "UP") extends HealthChecker {

  override def check: Boolean = status === "UP"

}
