package commoncontext.usecases

import commoncontext.domain.gateways.HealthChecker

class CheckHealth(healthChecker: HealthChecker) {

  def check: String =
    if (healthChecker.check)
      "UP"
    else
      "DOWN"

}
