package commoncontext.usecases

import commoncontext.domain.entities.{AuthorizedUser, GuestUser}
import commoncontext.domain.gateways.IdentifierChecker
import scalaz.Scalaz._
import scalaz.Validation

class Gatekeeper(identifierLoader: IdentifierChecker) {

  def accessToAuthorizedContent(authorization: Option[String]): Validation[String, AuthorizedUser] =
    manageContentAccess(GuestUser success)(authorization)

  def accessToAuthenticatedContent(authorization: Option[String]): Validation[String, AuthorizedUser] =
    manageContentAccess("Forbidden" failure)(authorization)

  private def manageContentAccess(g: Validation[String, AuthorizedUser])(authorization: Option[String]): Validation[String, AuthorizedUser] = {
    authorization match {
      case Some(identifier) =>
        identifier.replace("Bearer ", "") match {
          case "guest" => g
          case identifier: String => identifierLoader.check(identifier) match {
            case Some(authenticatedUser) => authenticatedUser success
            case _ => "Unauthorized" failure
          }
        }
      case _ => "Unauthorized" failure
    }
  }

}
