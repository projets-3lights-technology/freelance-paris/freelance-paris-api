package commoncontext

import javax.inject.Inject

import play.api.mvc._

class Application @Inject()(cc: ControllerComponents) extends AbstractController(cc) {
  def unTrail(path: String) = Action {
    MovedPermanently("/" + path)
  }
}
