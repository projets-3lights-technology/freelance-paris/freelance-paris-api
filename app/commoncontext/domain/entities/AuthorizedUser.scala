package commoncontext.domain.entities

trait AuthorizedUser {
  def id: String

  def firstName: String

  def lastName: String

  def email: String

  def avatar: String

  def profileUrl: String
}

case class AuthenticatedUser(id: String,
                             firstName: String,
                             lastName: String,
                             email: String,
                             avatar: String,
                             profileUrl: String) extends AuthorizedUser

case object GuestUser extends AuthorizedUser {
  def id: String = "guestUser"

  def firstName: String = "Guest"

  def lastName: String = "Guest"

  def email: String = "guest@freelance-paris.com"

  def avatar: String = ""

  def profileUrl: String = ""
}
