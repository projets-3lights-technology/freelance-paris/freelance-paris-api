package commoncontext.domain.gateways

trait HealthChecker {

  def check: Boolean

}
