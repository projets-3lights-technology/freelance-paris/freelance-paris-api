package commoncontext.domain.gateways

import commoncontext.domain.entities.AuthenticatedUser

trait IdentifierChecker {

  def check(identifier: String): Option[AuthenticatedUser]

}
