package commoncontext.domain.handlers

import scalaz.Validation

trait CommandHandler[C] {

  def handle(item: C): Validation[String, Boolean]

}
