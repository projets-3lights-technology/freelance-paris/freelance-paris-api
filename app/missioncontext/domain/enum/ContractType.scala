package missioncontext.domain.enum

object ContractType extends Enumeration {
  type Type = Value
  val APPRENTICESHIP, CDD, CDI, FREELANCE, INTERIM, OTHER, PORTAGE = Value

  implicit def toEnum(s: String): Type = s toUpperCase match {
    case "APPRENTICESHIP" => APPRENTICESHIP
    case "CDD" => CDD
    case "CDI" => CDI
    case "FREELANCE" => FREELANCE
    case "INTERN" => INTERIM
    case "PORTAGE" => PORTAGE
    case _ => OTHER
  }

  implicit def toString(e: ContractType.Type): String = e match {
    case APPRENTICESHIP => "APPRENTICESHIP"
    case CDD => "CDD"
    case CDI => "CDI"
    case FREELANCE => "FREELANCE"
    case INTERIM => "INTERIM"
    case PORTAGE => "PORTAGE"
    case OTHER => "OTHER"
  }
}
