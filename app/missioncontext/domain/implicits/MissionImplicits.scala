package missioncontext.domain.implicits

import missioncontext.domain.commands.PublishMissionCommand
import missioncontext.domain.entities._

object MissionImplicits {

  implicit def toMission(missionCommand: PublishMissionCommand): Mission = Mission(
    id = missionCommand.id,
    name = missionCommand.name,
    description = missionCommand.description,
    averageDailyRate = missionCommand.averageDailyRate,
    customer = missionCommand.customer,
    location = missionCommand.location,
    startDate = missionCommand.startDate,
    durationInMonths = missionCommand.durationInMonths,
    remoteIsPossible = missionCommand.remoteIsPossible,
    authorId = missionCommand.authorId,
    contractType = missionCommand.contractType,
    createdAt = missionCommand.createdAt
  )

  implicit def toMissionHeader(mission: Mission): MissionHeader = MissionHeader(
    id = mission.id,
    name = mission.name,
    description = mission.description,
    averageDailyRate = mission.averageDailyRate,
    authorId = mission.authorId,
    startDate = mission.startDate,
    createdAt = mission.createdAt
  )

  implicit def toMissionDetails(mission: Mission): MissionDetails = MissionDetails(
    id = mission.id,
    name = mission.name,
    description = mission.description,
    averageDailyRate = mission.averageDailyRate,
    customer = mission.customer,
    location = mission.location,
    startDate = mission.startDate,
    durationInMonths = mission.durationInMonths,
    remoteIsPossible = mission.remoteIsPossible,
    authorId = mission.authorId,
    contractType = mission.contractType,
    contactedBy = List.empty
  )

}
