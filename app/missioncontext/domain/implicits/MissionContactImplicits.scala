package missioncontext.domain.implicits

import missioncontext.domain.commands.ContactMissionCommand
import missioncontext.domain.entities._

object MissionContactImplicits {

  implicit def toContactItem(contactMissionCommand: ContactMissionCommand): ContactItem = ContactItem(
    subject = contactMissionCommand.subjectToSend,
    message = contactMissionCommand.messageToSend,
    author = contactMissionCommand.author,
    recipient = contactMissionCommand.recipient
  )

  implicit def toMissionContact(contactMissionCommand: ContactMissionCommand): MissionContact = MissionContact(
    missionId = contactMissionCommand.missionId,
    authorId = contactMissionCommand.author.id,
    subject = contactMissionCommand.subjectToSend,
    message = contactMissionCommand.messageToSend,
    authorEmail = contactMissionCommand.author.email,
    recipientEmail = contactMissionCommand.recipient.email,
    sentAt = contactMissionCommand.sentAt
  )

}
