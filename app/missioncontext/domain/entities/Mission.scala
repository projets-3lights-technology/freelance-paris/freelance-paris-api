package missioncontext.domain.entities

import java.util.UUID

import missioncontext.domain.enum.ContractType
import org.joda.time.DateTime

case class Mission(id: UUID,
                   name: String,
                   description: String,
                   averageDailyRate: Int,
                   customer: String,
                   location: String,
                   startDate: DateTime,
                   durationInMonths: String,
                   remoteIsPossible: Boolean,
                   authorId: String,
                   contractType: ContractType.Type,
                   createdAt: DateTime)
