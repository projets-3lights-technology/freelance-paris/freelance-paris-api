package missioncontext.domain.entities

import java.util.UUID

import org.joda.time.DateTime

case class MissionHeader(id: UUID,
                         name: String,
                         description: String,
                         averageDailyRate: Int,
                         authorId: String,
                         startDate: DateTime,
                         createdAt: DateTime = DateTime.now())
  extends Ordered[MissionHeader] {

  override def compare(that: MissionHeader): Int =
    if (this.createdAt.isEqual(that.createdAt.toInstant))
      0
    else if (this.createdAt.isBefore(that.createdAt.toInstant))
      1
    else
      -1

}
