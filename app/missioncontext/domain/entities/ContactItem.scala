package missioncontext.domain.entities

case class ContactItem(subject: String,
                       message: String,
                       author: Author,
                       recipient: Recipient)
