package missioncontext.domain.entities

case class Recipient(id: String,
                     firstName: String,
                     lastName: String,
                     email: String)
