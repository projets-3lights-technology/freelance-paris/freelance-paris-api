package missioncontext.domain.entities

import java.util.UUID

import org.joda.time.DateTime

case class MissionContact(missionId: UUID,
                          authorId: String,
                          subject: String,
                          message: String,
                          authorEmail: String,
                          recipientEmail: String,
                          sentAt: DateTime)
