package missioncontext.domain.entities

case class Author(id: String,
                  firstName: String,
                  lastName: String,
                  email: String,
                  profileUrl: String) {

  def fullName: String = firstName.concat(" ").concat(lastName)

}
