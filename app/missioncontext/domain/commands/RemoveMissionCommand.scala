package missioncontext.domain.commands

import java.util.UUID

import scalaz.Scalaz._

case class RemoveMissionCommand(missionId: UUID,
                                missionAuthorId: String,
                                memberRequestId: String) {

  def isPossible: Boolean = missionAuthorId === memberRequestId

}
