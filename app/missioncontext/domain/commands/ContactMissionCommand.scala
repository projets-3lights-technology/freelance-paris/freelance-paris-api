package missioncontext.domain.commands

import java.util.UUID

import missioncontext.domain.entities.{Author, Recipient}
import org.joda.time.DateTime

case class ContactMissionCommand(missionId: UUID,
                                 missionName: String,
                                 author: Author,
                                 recipient: Recipient,
                                 subject: String,
                                 message: String,
                                 sentAt: DateTime = DateTime.now()) {

  def subjectToSend: String = s"[$missionName]"

  def messageToSend: String =
      s"""
        | <p>${author.fullName} vous a contacté pour la mission suivante : $missionName</p>
        |
        | <p>Son profile LinkedIn : ${author.profileUrl}</p>
        |
        | <span>---</span>
        |
        | <p>$message</p>
        |
        | <p>Vous pouvez directement répondre à ${author.fullName} en répondant à ce message</p>
        |
        | <p>Cordialement,</p>
        | <b>Freelance Paris</b>
      """.stripMargin

}
