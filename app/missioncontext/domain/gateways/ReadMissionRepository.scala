package missioncontext.domain.gateways

import java.util.UUID

import missioncontext.domain.entities.{MissionDetails, MissionHeader}

trait ReadMissionRepository {

  def fetchAll: Set[MissionHeader]

  def getById(id: UUID): Option[MissionDetails]

}
