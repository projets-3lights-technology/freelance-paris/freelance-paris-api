package missioncontext.domain.gateways

import missioncontext.domain.entities.ContactItem

trait EmailNotifier {

  def send(item: ContactItem): Unit

}
