package missioncontext.domain.gateways

import java.util.UUID

import missioncontext.domain.entities.Mission
import scalaz.Validation

trait CommandMissionRepository {

  def add(item: Mission): Validation[String, Boolean]

  def remove(id: UUID): Validation[String, Boolean]

}
