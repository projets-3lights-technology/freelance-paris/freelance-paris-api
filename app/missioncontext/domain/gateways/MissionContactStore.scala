package missioncontext.domain.gateways

import missioncontext.domain.entities.MissionContact

trait MissionContactStore {

  def store(item: MissionContact): Unit

}
