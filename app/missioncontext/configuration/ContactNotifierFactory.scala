package missioncontext.configuration

import configuration.environment.AppConfiguration
import configuration.environment.ConfigurationKey._
import missioncontext.adapters.gateways.inmemory.InMemoryEmailNotifier
import missioncontext.adapters.gateways.real.JavaMailEmailNotifier
import missioncontext.domain.gateways.EmailNotifier

case class ContactNotifierFactory(appConfiguration: AppConfiguration) {

  def makeContactNotifier: EmailNotifier =
    appConfiguration.get(MODE) match {
      case Some("inmemory") => new InMemoryEmailNotifier()
      case _ => new JavaMailEmailNotifier(appConfiguration)
    }

}
