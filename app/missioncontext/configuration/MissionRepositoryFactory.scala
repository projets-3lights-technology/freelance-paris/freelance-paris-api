package missioncontext.configuration

import java.util.UUID

import configuration.environment.AppConfiguration
import configuration.environment.ConfigurationKey._
import missioncontext.adapters.gateways.inmemory.InMemoryMissionRepository
import missioncontext.adapters.gateways.real.PostgresMissionRepository
import missioncontext.domain.entities.{MemberId, MissionDetails, MissionHeader}
import missioncontext.domain.enum.ContractType._
import missioncontext.domain.gateways.{CommandMissionRepository, ReadMissionRepository}
import org.joda.time.DateTime

case class MissionRepositoryFactory(appConfiguration: AppConfiguration) {

  var inMemoryCommandMissionRepository: CommandMissionRepository = _
  var inMemoryReadMissionRepository: ReadMissionRepository = _

  def makeCommandMissionRepository: CommandMissionRepository =
    appConfiguration.get(MODE) match {
      case Some("inmemory") => {
        buildInMemoryMissionRepository
        inMemoryCommandMissionRepository
      }
      case _ => new PostgresMissionRepository(appConfiguration)
    }

  def makeReadMissionRepository: ReadMissionRepository =
    appConfiguration.get(MODE) match {
      case Some("inmemory") => {
        buildInMemoryMissionRepository
        inMemoryReadMissionRepository
      }
      case _ => new PostgresMissionRepository(appConfiguration)
    }

  private def buildInMemoryMissionRepository: Unit = {
    val uuid1 = UUID.fromString("0-0-0-0-1")
    val uuid2 = UUID.randomUUID()

    val missionsHeaderPopulation = Set(
      MissionHeader(
        id = uuid1,
        name = "Name",
        description = "Lorem ipsum",
        averageDailyRate = 1234,
        authorId = "authorId",
        startDate = DateTime.now().plusDays(2),
        createdAt = DateTime.now().plusDays(1)
      ),
      MissionHeader(
        id = uuid2,
        name = "Mission",
        description = "Lorem ipsum",
        averageDailyRate = 1234,
        authorId = "id",
        startDate = DateTime.now().plusDays(1),
        createdAt = DateTime.now()
      )
    )
    val missionsDetailsPopulation = Set(
      MissionDetails(
        id = uuid1,
        name = "Name",
        description = "Lorem ipsum",
        averageDailyRate = 1234,
        customer = "",
        location = "location",
        startDate = DateTime.now().plusDays(2),
        durationInMonths = "-1",
        remoteIsPossible = false,
        authorId = "authorId",
        contractType = FREELANCE,
        contactedBy = List(MemberId("registeredUser"))
      ),
      MissionDetails(
        id = uuid2,
        name = "Mission",
        description = "Lorem ipsum",
        averageDailyRate = 1234,
        customer = "customer",
        location = "location",
        startDate = DateTime.now().plusDays(1),
        durationInMonths = "1",
        remoteIsPossible = true,
        authorId = "id",
        contractType = APPRENTICESHIP,
        contactedBy = List.empty
      )
    )

    if (inMemoryReadMissionRepository == null && inMemoryCommandMissionRepository == null) {
      val inMemoryMissionRepository = new InMemoryMissionRepository(missionsHeaderPopulation, missionsDetailsPopulation)
      inMemoryReadMissionRepository = inMemoryMissionRepository
      inMemoryCommandMissionRepository = inMemoryMissionRepository
    }
  }

}
