package missioncontext.configuration.providers

import com.google.inject.{Inject, Provider}
import missioncontext.domain.gateways.{EmailNotifier, MissionContactStore}
import missioncontext.usecases.commands.ContactMission

class ContactMissionProvider @Inject()(contactNotifier: EmailNotifier,
                                       missionContactStore: MissionContactStore)
  extends Provider[ContactMission] {

  def get(): ContactMission = new ContactMission(contactNotifier, missionContactStore)

}
