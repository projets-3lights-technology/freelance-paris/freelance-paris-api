package missioncontext.configuration.providers

import com.google.inject.{Inject, Provider}
import missioncontext.domain.gateways.ReadMissionRepository
import missioncontext.usecases.queries.MissionReadModel

class MissionReadModelProvider @Inject()(readMissionRepository: ReadMissionRepository)
  extends Provider[MissionReadModel] {

  def get(): MissionReadModel = new MissionReadModel(readMissionRepository)

}
