package missioncontext.configuration.providers

import com.google.inject.{Inject, Provider}
import missioncontext.domain.gateways.CommandMissionRepository
import missioncontext.usecases.commands.PublishMission

class PublishMissionProvider @Inject()(commandMissionRepository: CommandMissionRepository)
  extends Provider[PublishMission] {

  def get(): PublishMission = new PublishMission(commandMissionRepository)

}
