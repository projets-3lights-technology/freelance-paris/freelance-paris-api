package missioncontext.configuration.providers

import com.google.inject.{Inject, Provider}
import missioncontext.domain.gateways.CommandMissionRepository
import missioncontext.usecases.commands.RemoveMission

class RemoveMissionProvider @Inject()(commandMissionRepository: CommandMissionRepository)
  extends Provider[RemoveMission] {

  def get(): RemoveMission = new RemoveMission(commandMissionRepository)

}
