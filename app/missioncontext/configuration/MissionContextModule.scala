package missioncontext.configuration

import com.google.inject.{AbstractModule, TypeLiteral}
import configuration.environment.EnvVarAppConfiguration
import missioncontext.configuration.providers.{ContactMissionProvider, MissionReadModelProvider, PublishMissionProvider, RemoveMissionProvider}
import missioncontext.domain.gateways.{CommandMissionRepository, EmailNotifier, MissionContactStore, ReadMissionRepository}
import missioncontext.usecases.commands.{ContactMission, PublishMission, RemoveMission}
import missioncontext.usecases.queries.MissionReadModel

class MissionContextModule extends AbstractModule {

  override def configure(): Unit = {
    val missionRepositoryFactory = MissionRepositoryFactory(EnvVarAppConfiguration)
    bind(new TypeLiteral[CommandMissionRepository] {}) toInstance missionRepositoryFactory.makeCommandMissionRepository
    bind(new TypeLiteral[ReadMissionRepository] {}) toInstance missionRepositoryFactory.makeReadMissionRepository
    bind(new TypeLiteral[EmailNotifier] {}) toInstance ContactNotifierFactory(EnvVarAppConfiguration).makeContactNotifier
    bind(new TypeLiteral[MissionContactStore] {}) toInstance MissionContactStoreFactory(EnvVarAppConfiguration).makeMissionContactStore

    bind(classOf[MissionReadModel]) toProvider classOf[MissionReadModelProvider] asEagerSingleton()
    bind(classOf[PublishMission]) toProvider classOf[PublishMissionProvider] asEagerSingleton()
    bind(classOf[RemoveMission]) toProvider classOf[RemoveMissionProvider] asEagerSingleton()
    bind(classOf[ContactMission]) toProvider classOf[ContactMissionProvider] asEagerSingleton()
  }

}
