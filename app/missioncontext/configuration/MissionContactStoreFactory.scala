package missioncontext.configuration

import configuration.environment.AppConfiguration
import configuration.environment.ConfigurationKey._
import missioncontext.adapters.gateways.inmemory.InMemoryMissionContactStore
import missioncontext.adapters.gateways.real.PostgresMissionContactStore
import missioncontext.domain.gateways.MissionContactStore

case class MissionContactStoreFactory(appConfiguration: AppConfiguration) {

  def makeMissionContactStore: MissionContactStore =
    appConfiguration.get(MODE) match {
      case Some("inmemory") => new InMemoryMissionContactStore()
      case _ => new PostgresMissionContactStore(appConfiguration)
    }

}
