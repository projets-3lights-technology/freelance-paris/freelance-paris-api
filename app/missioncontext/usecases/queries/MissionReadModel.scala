package missioncontext.usecases.queries

import java.util.UUID

import missioncontext.domain.entities.{MissionDetails, MissionHeader}
import missioncontext.domain.gateways.ReadMissionRepository
import org.joda.time.DateTime

class MissionReadModel(readMissionRepository: ReadMissionRepository) {

  def fetchStartedMissionStrictlyAfter(date: DateTime): List[MissionHeader] = {
    def removeOutdatedMission(m: MissionHeader): Boolean = m.startDate.isAfter(date.toInstant)

    readMissionRepository.fetchAll
      .filter(removeOutdatedMission)
      .toList
      .sorted
  }

  def getMissionById(id: UUID): Option[MissionDetails] = readMissionRepository getById id

}
