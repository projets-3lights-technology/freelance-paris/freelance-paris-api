package missioncontext.usecases.commands

import commoncontext.domain.handlers.CommandHandler
import missioncontext.domain.commands.PublishMissionCommand
import missioncontext.domain.gateways.CommandMissionRepository
import missioncontext.domain.implicits.MissionImplicits._
import scalaz.Validation

class PublishMission(commandMissionRepository: CommandMissionRepository)
  extends CommandHandler[PublishMissionCommand] {

  override def handle(item: PublishMissionCommand): Validation[String, Boolean] = commandMissionRepository.add(item)

}
