package missioncontext.usecases.commands

import commoncontext.domain.handlers.CommandHandler
import missioncontext.domain.commands.RemoveMissionCommand
import missioncontext.domain.gateways.CommandMissionRepository
import scalaz.Scalaz._
import scalaz.Validation

class RemoveMission(CommandMissionRepository: CommandMissionRepository)
  extends CommandHandler[RemoveMissionCommand] {

  override def handle(removeMissionCommand: RemoveMissionCommand): Validation[String, Boolean] =
    if (removeMissionCommand.isPossible)
      CommandMissionRepository.remove(removeMissionCommand.missionId)
    else
      "The member isn't the author of this mission" failure

}
