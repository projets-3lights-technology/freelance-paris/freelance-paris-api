package missioncontext.usecases.commands

import commoncontext.domain.handlers.CommandHandler
import missioncontext.domain.commands.ContactMissionCommand
import missioncontext.domain.gateways.{EmailNotifier, MissionContactStore}
import missioncontext.domain.implicits.MissionContactImplicits._
import scalaz.Scalaz._
import scalaz.Validation

class ContactMission(contactNotifier: EmailNotifier,
                     missionContactStore: MissionContactStore) extends CommandHandler[ContactMissionCommand] {

  override def handle(item: ContactMissionCommand): Validation[String, Boolean] = {
    contactNotifier.send(item)
    missionContactStore.store(item)
    true success
  }

}
