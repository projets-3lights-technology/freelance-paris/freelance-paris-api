package missioncontext.adapters.gateways.real.DTO

case class DBMissionHeaderDTO(uuid: String,
                              name: String,
                              description: String,
                              average_daily_rate: Int,
                              start_date: String,
                              author_id: String,
                              created_at: String)
