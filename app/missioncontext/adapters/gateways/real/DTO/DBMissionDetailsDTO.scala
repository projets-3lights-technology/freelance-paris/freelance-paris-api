package missioncontext.adapters.gateways.real.DTO

case class DBMissionDetailsDTO(uuid: String,
                               name: String,
                               description: String,
                               average_daily_rate: Int,
                               customer: String,
                               location: String,
                               start_date: String,
                               duration_in_months: String,
                               remote_is_possible: Boolean,
                               author_id: String,
                               contract_type: String,
                               contacted_by: Option[String])
