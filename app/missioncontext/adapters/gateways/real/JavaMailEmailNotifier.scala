package missioncontext.adapters.gateways.real

import java.util.{Date, Properties}

import configuration.environment.AppConfiguration
import configuration.environment.ConfigurationKey._
import javax.mail._
import javax.mail.internet.{InternetAddress, MimeMessage}
import missioncontext.domain.entities.{Author, ContactItem, Recipient}
import missioncontext.domain.gateways.EmailNotifier

class JavaMailEmailNotifier(appConfiguration: AppConfiguration) extends EmailNotifier {

  override def send(item: ContactItem): Unit = {
    try {
      Transport.send(createMimeMessage(item, createSession))
    } catch {
      case mex: MessagingException => println(mex)
    }
  }

  private def createSession: Session = {
    val props = new Properties()
    props.put("mail.smtp.auth", appConfiguration.get(MAIL_SMTP_AUTH).get)
    props.put("mail.smtp.starttls.enable", appConfiguration.get(MAIL_SMTP_STARTTLS).get)
    props.put("mail.smtp.host", appConfiguration.get(MAIL_SMTP_HOST).get)
    props.put("mail.smtp.port", appConfiguration.get(MAIL_SMTP_PORT).get)

    Session.getInstance(props, new Authenticator() {
      override protected def getPasswordAuthentication(): PasswordAuthentication =
        new PasswordAuthentication(
          appConfiguration.get(MAIL_AUTH_LOGIN).get,
          appConfiguration.get(MAIL_AUTH_PASSWORD).get
        )
    })
  }

  private def createMimeMessage(item: ContactItem, session: Session): MimeMessage = {
    val msg = new MimeMessage(session)
    msg.setFrom(fromAddress(item.author))
    msg.setRecipients(Message.RecipientType.TO, Array(recipientAddress(item.recipient)))

    msg.setReplyTo(Array(replyToAddress(item.author)))

    msg.setSubject(item.subject)
    msg.setSentDate(new Date())

    msg.setText(item.message, "utf-8", "html")

    msg
  }

  private def fromAddress(author: Author): Address =
    new InternetAddress(author.email, appConfiguration.get(MAIL_CONTACT_NAME).get).asInstanceOf[Address]

  private def recipientAddress(recipient: Recipient): Address =
    new InternetAddress(recipient.email).asInstanceOf[Address]

  private def replyToAddress(author: Author): Address =
    new InternetAddress(author.email, author.fullName).asInstanceOf[Address]

}
