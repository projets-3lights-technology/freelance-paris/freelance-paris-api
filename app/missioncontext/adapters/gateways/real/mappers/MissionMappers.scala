package missioncontext.adapters.gateways.real.mappers

import java.util.UUID

import missioncontext.adapters.gateways.real.DTO.{DBMissionDetailsDTO, DBMissionHeaderDTO}
import missioncontext.domain.entities.{MemberId, MissionDetails, MissionHeader}
import missioncontext.domain.enum.ContractType._
import org.joda.time.DateTime

// todo
object MissionMappers {

  implicit def toMissionHeader(rows: List[DBMissionHeaderDTO]): Set[MissionHeader] = rows.map(row => MissionHeader(
    id = UUID.fromString(row.uuid),
    name = row.name,
    description = row.description,
    averageDailyRate = row.average_daily_rate,
    authorId = row.author_id,
    startDate = DateTime.parse(row.start_date),
    createdAt = DateTime.parse(row.created_at)
  )).toSet

  implicit def toMissionDetails(row: DBMissionDetailsDTO): MissionDetails =
    MissionDetails(
      id = UUID.fromString(row.uuid),
      name = row.name,
      description = row.description,
      averageDailyRate = row.average_daily_rate,
      customer = row.customer,
      location = row.location,
      startDate = DateTime.parse(row.start_date),
      durationInMonths = row.duration_in_months,
      remoteIsPossible = row.remote_is_possible,
      authorId = row.author_id,
      contractType = row.contract_type,
      contactedBy = row.contacted_by.map(r => r.split(",").toList.map(MemberId)).getOrElse(List.empty)
    )

}
