package missioncontext.adapters.gateways.real

import java.util.UUID

import commoncontext.adapters.gateways.real.PostgresDataBaseConnection
import configuration.environment.AppConfiguration
import doobie.implicits._
import missioncontext.adapters.gateways.real.DTO.{DBMissionDetailsDTO, DBMissionHeaderDTO}
import missioncontext.adapters.gateways.real.mappers.MissionMappers._
import missioncontext.domain.entities.{Mission, MissionDetails, MissionHeader}
import missioncontext.domain.gateways.{CommandMissionRepository, ReadMissionRepository}
import scalaz.Validation

class PostgresMissionRepository(appConfiguration: AppConfiguration)
  extends PostgresDataBaseConnection(appConfiguration)
    with CommandMissionRepository
    with ReadMissionRepository {

  override def fetchAll: Set[MissionHeader] = {
    val request = {
      sql"""
           | SELECT uuid,
           |        name,
           |        description,
           |        average_daily_rate,
           |        start_date,
           |        author_id,
           |        created_at
           | FROM mission
        """.stripMargin
    }

    queryList(request.query[DBMissionHeaderDTO], Set.empty)
  }

  override def getById(id: UUID): Option[MissionDetails] = {
    val request =
      sql"""
           | SELECT  uuid,
           |         name,
           |         description,
           |         average_daily_rate,
           |         customer,
           |         location,
           |         start_date,
           |         duration_in_months,
           |         remote_is_possible,
           |         author_id,
           |         (SELECT name from contract_type ct WHERE ct.id = m.contract_type) as contract_type,
           |         (SELECT string_agg(mc.author_id, ',') FROM mission_contact mc WHERE mc.mission_id = m.uuid) as contacted_by
           | FROM mission m
           | WHERE uuid = ${id.toString}
      """.stripMargin

    queryOne(request.query[DBMissionDetailsDTO], None)
  }

  override def add(item: Mission): Validation[String, Boolean] = {
    val request =
      sql"""
           | INSERT INTO mission (
           |   uuid,
           |   name,
           |   description,
           |   average_daily_rate,
           |   customer,
           |   location,
           |   start_date,
           |   duration_in_months,
           |   remote_is_possible,
           |   author_id,
           |   contract_type,
           |   created_at
           | )
           | VALUES (
           |   ${item.id.toString},
           |   ${item.name},
           |   ${item.description},
           |   ${item.averageDailyRate},
           |   ${item.customer},
           |   ${item.location},
           |   ${item.startDate.toString},
           |   ${item.durationInMonths},
           |   ${item.remoteIsPossible},
           |   ${item.authorId},
           |   (SELECT id FROM contract_type ct WHERE ct.name = ${item.contractType.toString}),
           |   ${item.createdAt.toString}
           | )
      """.stripMargin

    insert(request)
  }

  override def remove(id: UUID): Validation[String, Boolean] = {
    val request =
      sql"""
           | DELETE FROM mission
           | WHERE uuid = ${id.toString}
      """.stripMargin

    delete(request)
  }

}
