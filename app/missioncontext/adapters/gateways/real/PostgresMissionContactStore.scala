package missioncontext.adapters.gateways.real

import commoncontext.adapters.gateways.real.PostgresDataBaseConnection
import configuration.environment.AppConfiguration
import doobie.implicits._
import missioncontext.domain.entities.MissionContact
import missioncontext.domain.gateways.MissionContactStore

class PostgresMissionContactStore(appConfiguration: AppConfiguration)
  extends PostgresDataBaseConnection(appConfiguration)
    with MissionContactStore {

  override def store(item: MissionContact): Unit = {
    val request =
      sql"""
           | INSERT INTO mission_contact (
           |   mission_id,
           |   author_id,
           |   subject,
           |   message,
           |   sender_email,
           |   recipient_email,
           |   sent_at
           | )
           | VALUES (
           |   ${item.missionId.toString},
           |   ${item.authorId},
           |   ${item.subject},
           |   ${item.message},
           |   ${item.authorEmail},
           |   ${item.recipientEmail},
           |   ${item.sentAt.toString}
           | )
      """.stripMargin

    insert(request)
  }

}
