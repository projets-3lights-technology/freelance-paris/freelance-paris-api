package missioncontext.adapters.gateways.inmemory

import java.util.UUID

import missioncontext.domain.entities.{Mission, MissionDetails, MissionHeader}
import missioncontext.domain.gateways.{CommandMissionRepository, ReadMissionRepository}
import missioncontext.domain.implicits.MissionImplicits._
import scalaz.Scalaz._
import scalaz.Validation

class InMemoryMissionRepository(missionsHeaderPopulation: Set[MissionHeader] = Set.empty,
                                missionDetailsPopulation: Set[MissionDetails] = Set.empty)
  extends CommandMissionRepository
    with ReadMissionRepository {

  private var missions: Set[MissionHeader] = missionsHeaderPopulation
  private var missionsDetails: Set[MissionDetails] = missionDetailsPopulation

  override def fetchAll: Set[MissionHeader] = missions

  override def getById(id: UUID): Option[MissionDetails] = missionsDetails.find(_.id equals id)

  override def add(item: Mission): Validation[String, Boolean] = getById(item.id) match {
    case None => {
      missions = missions + item
      missionsDetails = missionsDetails + item
      true success
    }
    case _ => item.id.toString + " already exists" failure
  }

  override def remove(id: UUID): Validation[String, Boolean] = {
    missions = missions.filterNot(_.id equals id)
    missionsDetails = missionsDetails.filterNot(_.id equals id)
    true success
  }

}
