package missioncontext.adapters.gateways.inmemory

import missioncontext.domain.entities.MissionContact
import missioncontext.domain.gateways.MissionContactStore

class InMemoryMissionContactStore extends MissionContactStore {

  var storeWith: MissionContact = _

  override def store(item: MissionContact): Unit = storeWith = item
}
