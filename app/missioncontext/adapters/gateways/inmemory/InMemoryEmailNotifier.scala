package missioncontext.adapters.gateways.inmemory

import missioncontext.domain.entities.ContactItem
import missioncontext.domain.gateways.EmailNotifier

class InMemoryEmailNotifier extends EmailNotifier {

  var notifyWith: ContactItem = _

  override def send(item: ContactItem): Unit = notifyWith = item

}
