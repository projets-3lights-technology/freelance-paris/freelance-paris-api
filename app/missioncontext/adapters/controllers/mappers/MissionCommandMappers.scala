package missioncontext.adapters.controllers.mappers

import commoncontext.domain.entities.AuthorizedUser
import membercontext.domain.entities.Member
import missioncontext.adapters.controllers.validators.ContactMissionForm
import missioncontext.domain.commands.ContactMissionCommand
import missioncontext.domain.entities.{Author, MissionDetails, Recipient}

object MissionCommandMappers {

  def toContactMissionCommand(mission: MissionDetails,
                              author: AuthorizedUser,
                              recipient: Member,
                              contactMissionForm: ContactMissionForm): ContactMissionCommand =
    ContactMissionCommand(
      missionId = mission.id,
      missionName = mission.name,
      author = Author(
        id = author.id,
        firstName = author.firstName,
        lastName = author.lastName,
        email = author.email,
        profileUrl = author.profileUrl
      ),
      recipient = Recipient(
        id = recipient.id,
        firstName = recipient.firstName,
        lastName = recipient.lastName,
        email = recipient.email
      ),
      subject = contactMissionForm.subject trim,
      message = contactMissionForm.message trim,
    )


}
