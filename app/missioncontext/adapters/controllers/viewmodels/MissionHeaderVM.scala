package missioncontext.adapters.controllers.viewmodels

import java.util.UUID

case class MissionHeaderVM(id: UUID,
                           name: String,
                           description: String,
                           average_daily_rate: Int,
                           author_id: String)
