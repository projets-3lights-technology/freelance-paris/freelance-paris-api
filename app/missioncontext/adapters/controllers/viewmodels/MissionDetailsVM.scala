package missioncontext.adapters.controllers.viewmodels

import java.util.UUID

import missioncontext.domain.enum.ContractType

case class MissionDetailsVM(id: UUID,
                            name: String,
                            description: String,
                            average_daily_rate: Int,
                            customer: String,
                            location: String,
                            start_date: String,
                            duration_in_months: String,
                            remote_is_possible: Boolean,
                            contract_type: ContractType.Type,
                            already_contacted: Boolean)
