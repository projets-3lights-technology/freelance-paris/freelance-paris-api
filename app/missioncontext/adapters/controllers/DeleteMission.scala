package missioncontext.adapters.controllers

import java.util.UUID

import commoncontext.domain.entities.AuthorizedUser
import commoncontext.usecases.Gatekeeper
import javax.inject.Inject
import missioncontext.domain.commands.RemoveMissionCommand
import missioncontext.usecases.commands.RemoveMission
import missioncontext.usecases.queries.MissionReadModel
import play.api.mvc._

class DeleteMission @Inject()(cc: ControllerComponents,
                              gatekeeper: Gatekeeper,
                              removeMission: RemoveMission,
                              missionReadModel: MissionReadModel) extends AbstractController(cc) {

  def remove(missionId: String) = Action { implicit request =>
    gatekeeper.accessToAuthenticatedContent(request.headers.get("Authorization"))
      .fold(
        {
          case "Forbidden" => Forbidden
          case _ => Unauthorized
        },
        doRemove(missionId)
      )
  }

  private def doRemove(missionId: String)(user: AuthorizedUser): Result = {
    missionReadModel.getMissionById(UUID.fromString(missionId)).
      map(mission => {
        val removeMissionCommand: RemoveMissionCommand = RemoveMissionCommand(
          missionId = UUID.fromString(missionId),
          missionAuthorId = mission.authorId,
          memberRequestId = user.id
        )
        removeMission.handle(removeMissionCommand)
      })
    NoContent
  }

}
