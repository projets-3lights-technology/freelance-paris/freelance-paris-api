package missioncontext.adapters.controllers.presenters

import missioncontext.adapters.controllers.viewmodels._
import missioncontext.domain.entities.{MemberId, MissionDetails, MissionHeader}

object MissionPresenter {

  implicit def toMissionsHeaderVM(ms: List[MissionHeader]): List[MissionHeaderVM] = ms.map(mission =>
    MissionHeaderVM(
      id = mission.id,
      name = mission.name,
      description = mission.description,
      average_daily_rate = mission.averageDailyRate,
      author_id = mission.authorId
    )
  )

  implicit def toMissionDetailsVM(t: (Option[MissionDetails], String)): Option[MissionDetailsVM] = t._1.map(mission =>
    MissionDetailsVM(
      id = mission.id,
      name = mission.name,
      description = mission.description,
      average_daily_rate = mission.averageDailyRate,
      customer = mission.customer,
      location = mission.location,
      start_date = mission.startDate.toString(),
      duration_in_months = mission.durationInMonths,
      remote_is_possible = mission.remoteIsPossible,
      contract_type = mission.contractType,
      already_contacted = mission.contactedBy.contains(MemberId(t._2))
    )
  )

}
