package missioncontext.adapters.controllers.validators

import java.util.{Date, UUID}

import missioncontext.domain.commands.PublishMissionCommand
import missioncontext.domain.enum.ContractType
import org.joda.time.DateTime

case class MissionForm(id: UUID,
                       name: String,
                       description: String,
                       average_daily_rate: Int,
                       customer: String,
                       location: String,
                       start_date: Date,
                       duration_in_months: String,
                       remote_is_possible: Boolean,
                       contract_type: ContractType.Type) {

  def toPublishMissionCommand(authorId: String): PublishMissionCommand = PublishMissionCommand(
    id = this.id,
    name = this.name trim,
    description = this.description trim,
    averageDailyRate = this.average_daily_rate,
    customer = this.customer trim,
    location = this.location trim,
    startDate = new DateTime(this.start_date),
    durationInMonths = this.duration_in_months trim,
    remoteIsPossible = this.remote_is_possible,
    authorId = authorId,
    contractType = this.contract_type
  )

}
