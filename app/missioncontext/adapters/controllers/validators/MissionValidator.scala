package missioncontext.adapters.controllers.validators

import commoncontext.adapters.controllers.FormMapping.EnumMapping._
import missioncontext.domain.enum.ContractType
import play.api.data.Forms._
import play.api.data._

// todo: test
object MissionValidator {

  def map: Form[MissionForm] = Form(
    mapping(
      "id" -> uuid,
      "name" -> nonEmptyText,
      "description" -> nonEmptyText,
      "average_daily_rate" -> number,
      "customer" -> text,
      "location" -> nonEmptyText,
      "start_date" -> date("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"),
      "duration_in_months" -> nonEmptyText,
      "remote_is_possible" -> boolean,
      "contract_type" -> enum(ContractType)
    )(MissionForm.apply)(MissionForm.unapply)
  )

}
