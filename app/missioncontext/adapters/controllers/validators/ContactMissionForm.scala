package missioncontext.adapters.controllers.validators

case class ContactMissionForm(subject: String,
                              message: String)