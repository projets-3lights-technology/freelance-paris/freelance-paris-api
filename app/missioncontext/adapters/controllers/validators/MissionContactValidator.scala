package missioncontext.adapters.controllers.validators

import play.api.data.Forms._
import play.api.data._

// todo: test
object MissionContactValidator {

  def map: Form[ContactMissionForm] = Form(
    mapping(
      "subject" -> nonEmptyText,
      "message" -> nonEmptyText
    )(ContactMissionForm.apply)(ContactMissionForm.unapply)
  )

}
