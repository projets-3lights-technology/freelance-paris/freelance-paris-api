package missioncontext.adapters.controllers

import commoncontext.adapters.controllers.presenters.ErrorPresenter._
import commoncontext.adapters.controllers.viewmodels.ErrorVM
import commoncontext.domain.entities.AuthorizedUser
import commoncontext.usecases.Gatekeeper
import javax.inject.Inject
import missioncontext.adapters.controllers.validators.{MissionForm, MissionValidator}
import missioncontext.usecases.commands.PublishMission
import play.api.libs.json._
import play.api.mvc._

class CreateMission @Inject()(cc: ControllerComponents,
                              gatekeeper: Gatekeeper,
                              publishMission: PublishMission) extends AbstractController(cc) {

  private implicit val writeError: OWrites[ErrorVM] = Json.writes[ErrorVM]

  def create = Action { implicit request =>
    gatekeeper.accessToAuthenticatedContent(request.headers.get("Authorization"))
      .fold(
        {
          case "Forbidden" => Forbidden
          case _ => Unauthorized
        },
        doCreate
      )
  }

  private def doCreate(author: AuthorizedUser)(implicit request: Request[_]): Result = MissionValidator.map.bindFromRequest
    .fold(
      form => BadRequest(Json.toJson(to400ErrorVM(form))),
      tryToPublishMission(author)
    )

  private def tryToPublishMission(author: AuthorizedUser)(missionForm: MissionForm): Result = publishMission
    .handle(missionForm.toPublishMissionCommand(author.id))
    .fold(
      error => Conflict(Json.toJson(to409ErrorVM(error))),
      _ => Created
    )

}
