package missioncontext.adapters.controllers

import commoncontext.usecases.Gatekeeper
import javax.inject.Inject
import missioncontext.adapters.controllers.presenters.MissionPresenter._
import missioncontext.adapters.controllers.viewmodels.MissionHeaderVM
import missioncontext.usecases.queries.MissionReadModel
import org.joda.time.DateTime
import play.api.libs.json._
import play.api.mvc.{AbstractController, ControllerComponents, Result}

class FetchStartedMissionsStrictlyAfterADate @Inject()(cc: ControllerComponents,
                                                       gatekeeper: Gatekeeper,
                                                       readModel: MissionReadModel) extends AbstractController(cc) {

  private implicit val writesMissionHeader: OWrites[MissionHeaderVM] = Json.writes[MissionHeaderVM]

  def fetch = Action { implicit request =>
    gatekeeper.accessToAuthorizedContent(request.headers.get("Authorization"))
      .fold(
        _ => Unauthorized,
        _ => doFetch
      )
  }

  private def doFetch: Result = {
    val missions: List[MissionHeaderVM] = readModel fetchStartedMissionStrictlyAfter DateTime.now()

    Ok(Json.toJson(missions))
  }

}
