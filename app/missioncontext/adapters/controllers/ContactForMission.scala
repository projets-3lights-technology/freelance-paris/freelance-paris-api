package missioncontext.adapters.controllers

import java.util.UUID

import commoncontext.adapters.controllers.presenters.ErrorPresenter._
import commoncontext.adapters.controllers.viewmodels.ErrorVM
import commoncontext.domain.entities.AuthorizedUser
import commoncontext.usecases.Gatekeeper
import javax.inject.Inject
import membercontext.usecases.queries.MemberReadModel
import missioncontext.adapters.controllers.mappers.MissionCommandMappers._
import missioncontext.adapters.controllers.validators.{ContactMissionForm, MissionContactValidator}
import missioncontext.usecases.commands.ContactMission
import missioncontext.usecases.queries.MissionReadModel
import play.api.libs.json._
import play.api.mvc.{AbstractController, ControllerComponents, Request, Result}

class ContactForMission @Inject()(cc: ControllerComponents,
                                  gatekeeper: Gatekeeper,
                                  memberReadModel: MemberReadModel,
                                  missionReadModel: MissionReadModel,
                                  contactMission: ContactMission) extends AbstractController(cc) {

  private implicit val writeError: OWrites[ErrorVM] = Json.writes[ErrorVM]

  def contact(missionId: String) = Action { implicit request =>
    gatekeeper.accessToAuthenticatedContent(request.headers.get("Authorization"))
      .fold(
        {
          case "Forbidden" => Forbidden
          case _ => Unauthorized
        },
        doContact(missionId)
      )
  }

  private def doContact(missionId: String)(author: AuthorizedUser)(implicit request: Request[_]): Result =
    MissionContactValidator.map.bindFromRequest
      .fold(
        form => BadRequest(Json.toJson(to400ErrorVM(form))),
        tryToSendAMissionContact(author)(missionId)
      )

  private def tryToSendAMissionContact(author: AuthorizedUser)(missionId: String)(contactMissionForm: ContactMissionForm): Result = {
    val information = for {
      mission <- missionReadModel getMissionById UUID.fromString(missionId)
      recipient <- memberReadModel getMemberById mission.authorId
    } yield toContactMissionCommand(mission, author, recipient, contactMissionForm)

    information match {
      case Some(c) => contactMission.handle(c)
        .fold(
          error => BadRequest(Json.toJson(to400ErrorVM(error))),
          _ => Created
        )
      case _ => BadRequest(Json.toJson(to400ErrorVM("Recipient email not found")))
    }
  }

}
