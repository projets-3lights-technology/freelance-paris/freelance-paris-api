package missioncontext.adapters.controllers

import java.util.UUID

import commoncontext.domain.entities.AuthorizedUser
import commoncontext.usecases.Gatekeeper
import javax.inject.Inject
import missioncontext.adapters.controllers.presenters.MissionPresenter._
import missioncontext.adapters.controllers.viewmodels.MissionDetailsVM
import missioncontext.usecases.queries.MissionReadModel
import play.api.libs.json._
import play.api.mvc.{AbstractController, ControllerComponents, Result}

class GetMissionById @Inject()(cc: ControllerComponents,
                               gatekeeper: Gatekeeper,
                               readModel: MissionReadModel) extends AbstractController(cc) {

  private implicit val writesMissionDetails: OWrites[MissionDetailsVM] = Json.writes[MissionDetailsVM]

  def getById(missionId: String) = Action { implicit request =>
    gatekeeper.accessToAuthorizedContent(request.headers.get("Authorization"))
      .fold(
        _ => Unauthorized,
        user => doGetById(missionId, user)
      )
  }

  private def doGetById(missionId: String, user: AuthorizedUser): Result = {
    val mission: Option[MissionDetailsVM] = (readModel getMissionById UUID.fromString(missionId)) -> user.id

    mission match {
      case None => NoContent
      case Some(_) => Ok(Json.toJson(mission))
    }
  }

}
