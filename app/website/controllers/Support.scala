package website.controllers

import javax.inject.Inject
import play.api.mvc._

class Support @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  def run = Action {
    Ok(views.html.support.render())
  }

}
