package website.controllers

import javax.inject.Inject
import play.api.mvc._

class Privacy @Inject()(cc: ControllerComponents) extends AbstractController(cc) {

  def run = Action {
    Ok(views.html.privacy.render())
  }

}
