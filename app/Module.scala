import com.google.inject.{AbstractModule, TypeLiteral}
import configuration.environment.{AppConfiguration, EnvVarAppConfiguration}

class Module extends AbstractModule {

  override def configure(): Unit = {
    bind(new TypeLiteral[AppConfiguration] {}) toInstance EnvVarAppConfiguration
  }

}
