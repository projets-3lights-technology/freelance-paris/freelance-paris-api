package configuration.environment

trait AppConfiguration {

  def get(key: ConfigurationKey.Type): Option[String]

}
