package configuration.environment

object EnvVarAppConfiguration extends AppConfiguration {

  override def get(key: ConfigurationKey.Type): Option[String] = sys.env.get(key)

}
