package membercontext.usecases

import commoncontext.domain.entities.AuthorizedUser
import commoncontext.domain.handlers.CommandHandler
import membercontext.domain.commands.CreateMemberCommand
import membercontext.domain.entities.Member
import membercontext.domain.implicits.MemberImplicits._
import membercontext.usecases.queries.MemberReadModel

class MemberOrchestrator(createMember: CommandHandler[CreateMemberCommand],
                         memberReadModel: MemberReadModel) {

  def handle(user: AuthorizedUser): Member = memberReadModel getMemberById user.id match {
    case Some(member) => Member(
      id = member.id,
      firstName = user.firstName,
      lastName = user.lastName,
      email = member.email,
      avatar = user.avatar,
      profileUrl = user.profileUrl
    )
    case None =>
      createMember.handle(user)
      user
  }

}
