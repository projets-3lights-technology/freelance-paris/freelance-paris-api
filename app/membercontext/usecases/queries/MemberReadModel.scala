package membercontext.usecases.queries

import membercontext.domain.entities.Member
import membercontext.domain.gateways.ReadMemberRepository

class MemberReadModel(readMemberRepository: ReadMemberRepository) {

  def getMemberById(id: String): Option[Member] = readMemberRepository geById id

}
