package membercontext.usecases.commands

import commoncontext.domain.handlers.CommandHandler
import membercontext.domain.commands.UpdateMemberEmailCommand
import membercontext.domain.gateways.CommandMemberRepository
import scalaz.Scalaz._
import scalaz.Validation

class UpdateMemberEmail(commandMemberRepository: CommandMemberRepository)
  extends CommandHandler[UpdateMemberEmailCommand] {

  override def handle(item: UpdateMemberEmailCommand): Validation[String, Boolean] = {
    val emailRegex = """^[a-zA-Z0-9\.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$""".r

    emailRegex.findFirstMatchIn(item.email) match {
      case Some(_) => commandMemberRepository.updateEmail(item.id, item.email)
      case _ => "Email is invalid" failure
    }
  }

}
