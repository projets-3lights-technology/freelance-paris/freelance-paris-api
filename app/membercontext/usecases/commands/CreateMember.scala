package membercontext.usecases.commands

import commoncontext.domain.handlers.CommandHandler
import membercontext.domain.commands.CreateMemberCommand
import membercontext.domain.gateways.CommandMemberRepository
import membercontext.domain.implicits.MemberImplicits._
import scalaz.Validation

class CreateMember(commandMemberRepository: CommandMemberRepository)
  extends CommandHandler[CreateMemberCommand] {

  override def handle(item: CreateMemberCommand): Validation[String, Boolean] = commandMemberRepository.add(item)

}
