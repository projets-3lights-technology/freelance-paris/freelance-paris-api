package membercontext.usecases.commands

import commoncontext.domain.handlers.CommandHandler
import membercontext.domain.commands.UnsubscribeMemberCommand
import membercontext.domain.gateways.CommandMemberRepository
import scalaz.Validation

class UnsubscribeMember(commandMemberRepository: CommandMemberRepository)
  extends CommandHandler[UnsubscribeMemberCommand] {

  override def handle(item: UnsubscribeMemberCommand): Validation[String, Boolean] =
    commandMemberRepository.remove(item.id)

}
