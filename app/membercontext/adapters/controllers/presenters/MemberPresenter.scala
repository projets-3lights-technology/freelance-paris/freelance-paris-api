package membercontext.adapters.controllers.presenters

import membercontext.adapters.controllers.viewmodels.MemberVM
import membercontext.domain.entities.Member

object MemberPresenter {

  implicit def toMemberVM(m: Member): MemberVM = MemberVM(
    id = m.id,
    first_name = m.firstName,
    last_name = m.lastName,
    email = m.email,
    avatar = m.avatar
  )

}
