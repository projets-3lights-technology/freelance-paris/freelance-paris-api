package membercontext.adapters.controllers.validators

import play.api.data.Forms._
import play.api.data._

// todo: test
object EmailValidator {

  def map: Form[EmailForm] = Form(
    mapping(
      "value" -> email,
    )(EmailForm.apply)(EmailForm.unapply)
  )

}
