package membercontext.adapters.controllers

import commoncontext.adapters.controllers.presenters.ErrorPresenter._
import commoncontext.adapters.controllers.viewmodels.ErrorVM
import commoncontext.domain.entities.AuthorizedUser
import commoncontext.usecases.Gatekeeper
import javax.inject.Inject
import membercontext.adapters.controllers.validators.{EmailForm, EmailValidator}
import membercontext.domain.commands.UpdateMemberEmailCommand
import membercontext.usecases.commands.UpdateMemberEmail
import play.api.libs.json._
import play.api.mvc.{AbstractController, ControllerComponents, Request, Result}

class UpdateEmail @Inject()(cc: ControllerComponents,
                            updateMemberEmail: UpdateMemberEmail,
                            gatekeeper: Gatekeeper) extends AbstractController(cc) {

  private implicit val writeError: OWrites[ErrorVM] = Json.writes[ErrorVM]

  def update = Action { implicit request =>
    gatekeeper.accessToAuthenticatedContent(request.headers.get("Authorization"))
      .fold(
        {
          case "Forbidden" => Forbidden
          case _ => Unauthorized
        },
        doUpdate
      )
  }

  def doUpdate(user: AuthorizedUser)(implicit request: Request[_]): Result = EmailValidator.map.bindFromRequest()
    .fold(
      form => BadRequest(Json.toJson(to400ErrorVM(form))),
      tryToUpdateMemberEmail(user)
    )

  private def tryToUpdateMemberEmail(user: AuthorizedUser)(emailForm: EmailForm): Result = {
    updateMemberEmail.handle(UpdateMemberEmailCommand(user.id, emailForm.value))
      .fold(
        error => BadRequest(Json.toJson(to400ErrorVM(error))),
        _ => Accepted
      )
  }

}
