package membercontext.adapters.controllers

import commoncontext.adapters.controllers.presenters.ErrorPresenter._
import commoncontext.adapters.controllers.viewmodels.ErrorVM
import commoncontext.domain.entities.AuthorizedUser
import commoncontext.usecases.Gatekeeper
import javax.inject.Inject
import membercontext.domain.commands.UnsubscribeMemberCommand
import membercontext.usecases.commands.UnsubscribeMember
import play.api.libs.json._
import play.api.mvc.{AbstractController, ControllerComponents, Result}

class Unsubscribe @Inject()(cc: ControllerComponents,
                            unsubscribeMember: UnsubscribeMember,
                            gatekeeper: Gatekeeper) extends AbstractController(cc) {

  private implicit val write: OWrites[ErrorVM] = Json.writes[ErrorVM]

  def unsubscribe = Action { implicit request =>
    gatekeeper.accessToAuthenticatedContent(request.headers.get("Authorization"))
      .fold(
        {
          case "Forbidden" => Forbidden
          case _ => Unauthorized
        },
        doUnsubscribe
      )
  }

  def doUnsubscribe(user: AuthorizedUser): Result = {
    unsubscribeMember.handle(UnsubscribeMemberCommand(user.id))
      .fold(
        error => BadRequest(Json.toJson(to400ErrorVM(error))),
        _ => NoContent
      )
  }

}
