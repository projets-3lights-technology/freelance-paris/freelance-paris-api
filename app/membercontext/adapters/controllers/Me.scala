package membercontext.adapters.controllers

import commoncontext.domain.entities.AuthorizedUser
import commoncontext.usecases.Gatekeeper
import javax.inject.Inject
import membercontext.adapters.controllers.presenters.MemberPresenter._
import membercontext.adapters.controllers.viewmodels.MemberVM
import membercontext.usecases.MemberOrchestrator
import play.api.libs.json._
import play.api.mvc.{AbstractController, ControllerComponents, Result}

class Me @Inject()(cc: ControllerComponents,
                   memberOrchestrator: MemberOrchestrator,
                   gatekeeper: Gatekeeper) extends AbstractController(cc) {

  private implicit val write: OWrites[MemberVM] = Json.writes[MemberVM]

  def fetch = Action { implicit request =>
    gatekeeper.accessToAuthorizedContent(request.headers.get("Authorization"))
      .fold(
        _ => Unauthorized,
        doFetch
      )
  }

  def doFetch(user: AuthorizedUser): Result = {
    val member: MemberVM = memberOrchestrator.handle(user)
    Ok(Json.toJson(member))
  }

}
