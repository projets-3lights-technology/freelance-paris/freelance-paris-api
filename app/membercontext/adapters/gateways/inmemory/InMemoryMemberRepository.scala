package membercontext.adapters.gateways.inmemory

import membercontext.domain.entities.Member
import membercontext.domain.gateways.{CommandMemberRepository, ReadMemberRepository}
import scalaz.Scalaz._
import scalaz.Validation

class InMemoryMemberRepository
  extends CommandMemberRepository
    with ReadMemberRepository {

  var addWith: Member = _
  var updateWith: Member = _
  var removeWith: String = _

  private var members: Set[Member] = Set.empty

  override def add(item: Member): Validation[String, Boolean] = {
    addWith = item
    members = members + item
    true success
  }

  override def updateEmail(id: String, email: String): Validation[String, Boolean] = {
    val member = members
      .find(_.id === id)
      .map(m => Member(
        id = m.id,
        firstName = m.firstName,
        lastName = m.lastName,
        email = email,
        avatar = m.avatar,
        profileUrl = m.profileUrl
      ))
    member match {
      case Some(m) => {
        members = members.filterNot(_.id === id) + m
        updateWith = m
        true success
      }
      case _ => "Member doesn't exist" failure
    }
  }

  override def remove(id: String): Validation[String, Boolean] = {
    removeWith = id
    members = members.filterNot(_.id === id)
    true success
  }

  override def geById(id: String): Option[Member] = members.find(_.id === id)

}
