package membercontext.adapters.gateways.real.DTO

case class DBMemberMeDTO(id: String,
                         first_name: String,
                         last_name: String,
                         email: String,
                         avatar: String,
                         profile_url: String)
