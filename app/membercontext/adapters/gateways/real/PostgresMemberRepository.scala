package membercontext.adapters.gateways.real

import commoncontext.adapters.gateways.real.PostgresDataBaseConnection
import configuration.environment.AppConfiguration
import doobie.implicits._
import membercontext.adapters.gateways.real.DTO.DBMemberMeDTO
import membercontext.adapters.gateways.real.mappers.MemberMappers._
import membercontext.domain.entities.Member
import membercontext.domain.gateways.{CommandMemberRepository, ReadMemberRepository}
import scalaz.Validation

class PostgresMemberRepository(appConfiguration: AppConfiguration)
  extends PostgresDataBaseConnection(appConfiguration)
    with CommandMemberRepository
    with ReadMemberRepository {

  override def add(item: Member): Validation[String, Boolean] = {
    val request =
      sql"""
           | INSERT INTO member (id, first_name, last_name, email, avatar, profile_url)
           | VALUES (
           |  ${item.id},
           |  ${item.firstName},
           |  ${item.lastName},
           |  ${item.email},
           |  ${item.avatar},
           |  ${item.profileUrl}
           | )
      """.stripMargin

    insert(request)
  }

  override def updateEmail(id: String, email: String): Validation[String, Boolean] = {
    val request =
      sql"""
           | UPDATE member
           | SET email = $email
           | WHERE id = $id
      """.stripMargin

    update(request)
  }

  override def remove(id: String): Validation[String, Boolean] = {
    val request =
      sql"""
           | DELETE FROM member
           | WHERE id = $id
      """.stripMargin

    delete(request)
  }

  override def geById(id: String): Option[Member] = {
    val request =
      sql"""
           | SELECT id, first_name, last_name, email, avatar, profile_url
           | FROM member
           | WHERE id = $id
      """.stripMargin

    queryOne(request.query[DBMemberMeDTO], None)
  }

}
