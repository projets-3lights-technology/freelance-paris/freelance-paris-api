package membercontext.adapters.gateways.real.mappers

import membercontext.adapters.gateways.real.DTO.DBMemberMeDTO
import membercontext.domain.entities.Member

// todo
object MemberMappers {

  implicit def toMember(row: DBMemberMeDTO): Member = Member(
    id = row.id,
    firstName = row.first_name,
    lastName = row.last_name,
    email = row.email,
    avatar = row.avatar,
    profileUrl = row.profile_url
  )

}
