package membercontext.domain.gateways

import membercontext.domain.entities.Member
import scalaz.Validation

trait CommandMemberRepository {

  def add(item: Member): Validation[String, Boolean]

  def updateEmail(id: String, email: String): Validation[String, Boolean]

  def remove(id: String): Validation[String, Boolean]

}
