package membercontext.domain.gateways

import membercontext.domain.entities.Member

trait ReadMemberRepository {

  def geById(id: String): Option[Member]

}
