package membercontext.domain.commands

case class CreateMemberCommand(id: String,
                               firstName: String,
                               lastName: String,
                               email: String,
                               avatar: String,
                               profileUrl: String)
