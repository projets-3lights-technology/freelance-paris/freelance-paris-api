package membercontext.domain.commands

case class UpdateMemberEmailCommand(id: String,
                                    email: String)
