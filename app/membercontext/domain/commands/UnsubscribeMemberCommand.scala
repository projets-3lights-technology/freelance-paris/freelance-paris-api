package membercontext.domain.commands

case class UnsubscribeMemberCommand(id: String)
