package membercontext.domain.implicits

import commoncontext.domain.entities.AuthorizedUser
import membercontext.domain.commands.CreateMemberCommand
import membercontext.domain.entities.Member

object MemberImplicits {

  implicit def toMember(memberCommand: CreateMemberCommand): Member = Member(
    id = memberCommand.id,
    firstName = memberCommand.firstName,
    lastName = memberCommand.lastName,
    email = memberCommand.email,
    avatar = memberCommand.avatar,
    profileUrl = memberCommand.profileUrl
  )

  implicit def toMember(user: AuthorizedUser): Member = Member(
    id = user.id,
    firstName = user.firstName,
    lastName = user.lastName,
    email = user.email,
    avatar = user.avatar,
    profileUrl = user.profileUrl
  )

  implicit def toCreateMemberCommand(user: AuthorizedUser): CreateMemberCommand = CreateMemberCommand(
    id = user.id,
    firstName = user.firstName,
    lastName = user.lastName,
    email = user.email,
    avatar = user.avatar,
    profileUrl = user.profileUrl
  )

}
