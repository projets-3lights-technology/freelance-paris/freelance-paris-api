package membercontext.domain.entities

case class Member(id: String,
                  firstName: String,
                  lastName: String,
                  email: String,
                  avatar: String,
                  profileUrl: String)
