package membercontext.configuration

import configuration.environment.AppConfiguration
import configuration.environment.ConfigurationKey._
import membercontext.adapters.gateways.inmemory.InMemoryMemberRepository
import membercontext.adapters.gateways.real.PostgresMemberRepository
import membercontext.domain.gateways.{CommandMemberRepository, ReadMemberRepository}

case class MemberRepositoryFactory(appConfiguration: AppConfiguration) {

  var inMemoryCommandMemberRepository: CommandMemberRepository = _
  var inMemoryReadMemberRepository: ReadMemberRepository = _

  def makeCommandMemberRepository: CommandMemberRepository =
    appConfiguration.get(MODE) match {
      case Some("inmemory") => {
        buildInMemoryMemberRepository
        inMemoryCommandMemberRepository
      }
      case _ => new PostgresMemberRepository(appConfiguration)
    }

  def makeReadMemberRepository: ReadMemberRepository =
    appConfiguration.get(MODE) match {
      case Some("inmemory") => {
        buildInMemoryMemberRepository
        inMemoryReadMemberRepository
      }
      case _ => new PostgresMemberRepository(appConfiguration)
    }

  private def buildInMemoryMemberRepository: Unit =
    if (inMemoryReadMemberRepository == null && inMemoryCommandMemberRepository == null) {
      val inMemoryMemberRepository = new InMemoryMemberRepository()
      inMemoryReadMemberRepository = inMemoryMemberRepository
      inMemoryCommandMemberRepository = inMemoryMemberRepository
    }

}
