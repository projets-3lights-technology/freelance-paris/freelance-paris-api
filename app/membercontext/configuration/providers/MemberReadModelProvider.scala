package membercontext.configuration.providers

import com.google.inject.{Inject, Provider}
import membercontext.domain.gateways.ReadMemberRepository
import membercontext.usecases.queries.MemberReadModel

class MemberReadModelProvider @Inject()(readMemberRepository: ReadMemberRepository)
  extends Provider[MemberReadModel] {

  def get(): MemberReadModel = new MemberReadModel(readMemberRepository)

}
