package membercontext.configuration.providers

import com.google.inject.{Inject, Provider}
import membercontext.domain.gateways.CommandMemberRepository
import membercontext.usecases.commands.CreateMember

class CreateMemberProvider @Inject()(commandMemberRepository: CommandMemberRepository)
  extends Provider[CreateMember] {

  def get(): CreateMember = new CreateMember(commandMemberRepository)

}
