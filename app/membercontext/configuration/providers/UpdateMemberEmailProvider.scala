package membercontext.configuration.providers

import com.google.inject.{Inject, Provider}
import membercontext.domain.gateways.CommandMemberRepository
import membercontext.usecases.commands.UpdateMemberEmail

class UpdateMemberEmailProvider @Inject()(commandMemberRepository: CommandMemberRepository)
  extends Provider[UpdateMemberEmail] {

  def get(): UpdateMemberEmail = new UpdateMemberEmail(commandMemberRepository)

}
