package membercontext.configuration.providers

import com.google.inject.{Inject, Provider}
import membercontext.usecases.MemberOrchestrator
import membercontext.usecases.commands.CreateMember
import membercontext.usecases.queries.MemberReadModel

class MemberOrchestratorProvider @Inject()(createMember: CreateMember,
                                           memberReadModel: MemberReadModel)
  extends Provider[MemberOrchestrator] {

  def get(): MemberOrchestrator = new MemberOrchestrator(createMember, memberReadModel)

}
