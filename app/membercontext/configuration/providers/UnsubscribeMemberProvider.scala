package membercontext.configuration.providers

import com.google.inject.{Inject, Provider}
import membercontext.domain.gateways.CommandMemberRepository
import membercontext.usecases.commands.UnsubscribeMember

class UnsubscribeMemberProvider @Inject()(commandMemberRepository: CommandMemberRepository)
  extends Provider[UnsubscribeMember] {

  def get(): UnsubscribeMember = new UnsubscribeMember(commandMemberRepository)

}
