package membercontext.configuration

import com.google.inject.{AbstractModule, TypeLiteral}
import configuration.environment.EnvVarAppConfiguration
import membercontext.configuration.providers._
import membercontext.domain.gateways.{CommandMemberRepository, ReadMemberRepository}
import membercontext.usecases.MemberOrchestrator
import membercontext.usecases.commands.{CreateMember, UnsubscribeMember, UpdateMemberEmail}
import membercontext.usecases.queries.MemberReadModel

class MemberContextModule extends AbstractModule {

  override def configure(): Unit = {
    val memberRepositoryFactory = MemberRepositoryFactory(EnvVarAppConfiguration)
    bind(new TypeLiteral[CommandMemberRepository] {}) toInstance memberRepositoryFactory.makeCommandMemberRepository
    bind(new TypeLiteral[ReadMemberRepository] {}) toInstance memberRepositoryFactory.makeReadMemberRepository

    bind(classOf[MemberReadModel]) toProvider classOf[MemberReadModelProvider] asEagerSingleton()
    bind(classOf[CreateMember]) toProvider classOf[CreateMemberProvider] asEagerSingleton()
    bind(classOf[UpdateMemberEmail]) toProvider classOf[UpdateMemberEmailProvider] asEagerSingleton()
    bind(classOf[UnsubscribeMember]) toProvider classOf[UnsubscribeMemberProvider] asEagerSingleton()
    bind(classOf[MemberOrchestrator]) toProvider classOf[MemberOrchestratorProvider] asEagerSingleton()
  }

}
